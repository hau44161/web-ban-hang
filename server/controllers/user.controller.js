const shortid = require("shortid");
const User = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports.login = (req, res, next) => {
  try {
    let email = req.body.email;
    let password = req.body.password;

    User.getOne(email, async (err, [data, ...re]) => {
      if (err) throw err;
      if (!data) return res.status(422).send("account is not exists");

      const checkPassword = await bcrypt.compare(password, data.password);

      if (!checkPassword)
        return res.status(422).send("Email or Password is not correct");

      const token = jwt.sign({ userId: data.id }, process.env.TOKEN_SECRET, {
        expiresIn: 60 * 60 * 24,
      });
      //res.append("Access-Control-Allow-Origin", ["*"]);
      res.append("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
      res.append("Access-Control-Allow-Headers", "Content-Type");
      res.append("Authentication", token);
      res.send({ ...data, password: "", token });
      // res.status(201).cookie("Authentication", token, { signed: true });
      // res.send({ ...data, password: "" });
    });
  } catch (err) {
    next(err);
  }
};
module.exports.logout = (req, res, next) => {
  try {
    //req.logout();
    //res.clearCookie("Authentication");
    res.status(202);
    res.send("Logout ok");
  } catch (err) {
    next(err);
  }
};

module.exports.remove = (req, res, next) => {
  try {
    let email = req.body.email;
    User.removeOne(email, (err, data) => {
      (err, data) => {
        if (err) res.status(422).send("Fail. cannot delete promotion");

        res.send("Success");
      };
    });
  } catch (err) {
    next(err);
  }
};

module.exports.add = async (req, res, next) => {
  try {
    let input = req.body;

    const salt = await bcrypt.genSalt(5);
    const hashPassword = await bcrypt.hash(input.password, salt);

    let records = [
      [
        shortid.generate(),
        input.name,
        hashPassword,
        input.email,
        input.sex,
        null,
      ],
    ];

    User.addOne(records, (err, data) => {
      if (err) {
        return res.status(422).send("Fail. cannot insert records");
      }
      res.send(data);
    });
  } catch (err) {
    next(err);
  }
};
