const shortid = require("shortid");
const conn = require("../mysql");

const tinh_tp = require("../locations/tinh_tp.json");
const quan_huyen = require("../locations/quan_huyen.json");
const xa_phuong = require("../locations/xa_phuong.json");

module.exports.getAll = (req, res, next) => {
  try {
    let sql =
      "select lc.id as id, ttp.name as tinhTp, qh.name as quanHuyen, px.name as phuongXa, lc.duongThon, lc.soNha " +
      "from locations lc, tinhTp ttp, quanHuyen qh, phuongXa px " +
      "where lc.tinhTp = ttp.id and lc.quanHuyen = qh.id and lc.phuongXa = px.id";

    conn.query(sql, (err, data) => {
      if (err) throw err;

      res.render("location.pug", { data });
    });
  } catch (err) {
    next(err);
  }
};
//locatin fw343g3
//provider 45t4ggt4
module.exports.remove = (req, res, next) => {
  try {
    conn.query(
      "DELETE FROM locations WHERE id = ?",
      req.params.id,
      (err, data) => {
        if (err) res.send("Fail. cannot delete promotion");
      }
    );

    res.redirect("/promotions");
  } catch (err) {
    next(err);
  }
};

module.exports.add = (req, res, next) => {
  try {
    let input = req.query;

    let records = [
      [
        shortid.generate(),
        input.tinhTp,
        input.quanHuyen,
        input.phuongXa,
        input.duongThon,
        input.soNha,
      ],
    ];

    conn.query("INSERT INTO locations VALUES ?", [records], (err, data) => {
      if (err) {
        res.send("Fail. cannot insert records");
        throw err;
      } else res.send(data);
    });
  } catch (err) {
    next(err);
  }
};

module.exports.getCode = (req, res, next) => {
  try {
    let input = req.query;
    let code;

    if (input == {} || true)
      code = Object.values(tinh_tp).map((c) => ({
        name: c.name,
        code: c.code,
      }));
    else if (input.tinhTp)
      code = Object.values(quan_huyen).map((c) => ({
        name: c.name,
        code: c.code,
      }));
    else if (input.quanHuyen)
      code = Object.values(xa_phuong).map((c) => ({
        name: c.name,
        code: c.code,
      }));

    res.send(code);
  } catch (err) {
    next(err);
  }
};

module.exports.setDataDefaults = (req, res) => {
  let records = {};

  records.tinhTp = Object.values(tinh_tp).map((c) => [
    parseInt(c.code),
    c.name,
  ]);
  records.quanHuyen = Object.values(quan_huyen).map((c) => [
    parseInt(c.code),
    c.name,
    parseInt(c.parent_code),
  ]);
  records.phuongXa = Object.values(xa_phuong).map((c) => [
    parseInt(c.code),
    c.name,
    parseInt(c.parent_code),
  ]);
  conn.query(
    "INSERT INTO phuongXa VALUES ?",
    [records.phuongXa],
    (err, data) => {
      if (err) {
        res.send("Fail. cannot insert records");
        throw err;
      } else res.send(data);
    }
  );
};
