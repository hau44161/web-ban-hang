const Product = require("../models/product.model");
const shortid = require("shortid");

module.exports.getAll = (req, res) => {
  let page = req.query.page || 1;
  Product.getProducts(page, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};

module.exports.getProductById = (req, res) => {
  let id = req.params.id || "";
  Product.getProductById(id, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};

module.exports.getProductDetailById = (req, res) => {
  let id = req.params.id || "";
  Product.getProductDetailById(id, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};

module.exports.search = (req, res) => {
  let keyword = req.query.keyword || "";
  Product.search(keyword, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};
module.exports.getProductHaveFlashSale = (req, res) => {
  Product.getProductHaveFlashSale((err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};

module.exports.getProductByCategory = (req, res) => {
  let id = req.params.id || "";
  Product.getProductByCategory(id, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};

module.exports.getProductByPromotion = (req, res) => {
  let id = req.params.id || "";
  Product.getProductByPromotion(id, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};

module.exports.add = (req, res) => {
  let input = req.body;
  let row = [
    shortid.generate(),
    input.location,
    parseInt(input.star),
    input.color,
    req.file.path.substr(7),
    input.name,
    parseFloat(input.price),
    input.idCategory,
    input.idProvider,
  ];

  Product.postProduct(row, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};

module.exports.remove = (req, res) => {
  let id = req.body.id || "";

  Product.delete(id, (err, data) => {
    if (err) throw err;
    res.send(JSON.stringify(data));
  });
};
