const Cart = require("../models/cart.model");
const jwt = require("jsonwebtoken");

module.exports.getAll = (req, res) => {
  let userId = req.userId;

  Cart.getAll(userId, (err, data) => {
    if (err) throw err;
    return res.send(data);
  });
};
module.exports.add = (req, res) => {
  let input = req.body;
  let userId = req.userId;

  let data = [input.idProduct, userId, input.count, input.count];
  Cart.add(data, (err, results) => {
    if (err) throw err;
    return res.send({ idProduct: data[0], idUser: data[1], count: data[2]});
  });
};
module.exports.remove = (req, res) => {
  let userId = req.userId;

  Cart.delete(userId, req.params.id, (err, data) => {
    if (err) throw err;
    if (data.affectedRows === 0) return res.status(100).send(data);
    else return res.status(200).send(data);
  });
};
module.exports.update = (req, res) => {
  let userId = req.userId;
  let input = req.body;

  let data = { idProduct: input.productId, idUser: userId, count: input.count };
  Cart.edit(data, (err, data) => {
    if (err) throw err;
    return res.send(data);
  });
};
