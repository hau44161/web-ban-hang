const conn = require("../mysql");
const Category = require("../models/category.model");

module.exports.getAll = (req, res) => {
  Category.getAll((err, data) => {
    if (err) throw err;

    res.send(JSON.stringify(data));
  });
};
