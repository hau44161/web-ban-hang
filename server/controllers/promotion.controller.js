const conn = require("../mysql");
const shortid = require("shortid");

module.exports.getAll = (req, res) => {
  try {
    conn.query("SELECT * FROM promotions", (err, data) => {
      res.render("promotion.pug", { data: data });
    });
  } catch (err) {
    throw err;
  }
};

module.exports.remove = (req, res, next) => {
  try {
    conn.query(
      "DELETE FROM promotions WHERE id = ?",
      req.params.id,
      (err, data) => {
        if (err) res.send("Fail. cannot delete promotion");
      }
    );
    res.redirect("/promotions");
  } catch (err) {
    next(err);
  }
};

module.exports.add = (req, res, next) => {
  try {
    let input = req.body;
    let records = [];
    records = [
      [
        input.name,
        input.percent ? input.percent : null,
        input.money_sale ? input.money_sale : null,
        input.freeship ? input.freeship : null,
      ],
    ];

    let data = records.map((c) => [shortid.generate(), ...c]);

    conn.query("INSERT INTO promotions VALUES ?", [data], (err, data) => {
      if (err) {
        res.send("Fail. data is not valid");
        throw err;
      }
    });
    res.redirect("/promotions");
    next();
  } catch (err) {
    next(err);
  }
};
