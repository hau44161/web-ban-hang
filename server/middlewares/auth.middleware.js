const jwt = require("jsonwebtoken");

module.exports.requireAuth = (req, res, next) => {
  try {
    let token = req.get("Authentication");

    if (!token) {
      return res.status(400).send("Access Denied");
    }
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.userId = verified.userId;
    
    next();
  } catch (err) {
    return res.status(400).send("Invalid Token");
  }
};
