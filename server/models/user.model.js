const conn = require("../mysql");

const User = {
  getAll: (callback) => {
    let sql = `SELECT * FROM users;`;
    conn.query(sql, callback);
  },
  getOne: (email, callback) => {
    let sql = `SELECT * FROM users WHERE email = '${email}';`;
    conn.query(sql, callback);
  },
  addOne: (records, callback) => {
    let sql = "INSERT INTO users VALUES ?";
    conn.query(sql, [records], callback);
  },
  removeOne: (email, callback) => {
    conn.query("DELETE FROM user WHERE email = ?", [email], callback);
  },
};

module.exports = User;
