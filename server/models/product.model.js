const conn = require("../mysql");

const Product = {
  search: (keyword, callback) => {
    let perPage = 24;
    let sql = `SELECT * FROM products WHERE name like '%${keyword}%'LIMIT ${perPage};`;
    conn.query(sql, callback);
  },

  getProducts: (page, callback) => {
    let perPage = 24;
    let start = (page - 1) * perPage;
    let sql = `SELECT * FROM products LIMIT ${start}, ${perPage};`;
    conn.query(sql, callback);
  },
  getProductHaveFlashSale: callback => {
    let sql = `SELECT p.* FROM products p 
              WHERE EXISTS 
                (SELECT * 
                FROM product_have_promotion pp, promotions pm 
                WHERE p.id=pp.idProduct and pp.idPromotion=pm.id and pm.percent>50);`
    conn.query(sql, callback);
  },
  getProductById: (id, callback) => {
    let sql = `SELECT * FROM products WHERE id = '${id}';`;
    conn.query(sql, callback);
  },

  getProductDetailById: (id, callback) => {
    let sql = `SELECT * FROM product_detail WHERE idProduct = '${id}';`;
    conn.query(sql, callback);
  },

  getProductByCategory: (id, callback) => {
    let perPage = 24;
    let sql = `SELECT * FROM products WHERE idCategory = '${id}' LIMIT ${perPage};`;

    conn.query(sql, callback);
  },

  getProductByPromotion: (id, callback) => {
    let perPage = 24;
    let sql = `SELECT * FROM products p
    WHERE EXISTS (SELECT * FROM product_have_promotion pp WHERE pp.idPromotion = '${id}' and p.id=pp.idProduct) LIMIT ${perPage};`;
    conn.query(sql, callback);
  },

  postProduct: ([id, location, star, color, ...product], callback) => {
    let productRow = [[id, ...product]];
    let productDetailRow = [[id, location, star, color]];

    conn.beginTransaction((err) => {
      if (err) throw err;
      conn.query("INSERT INTO products VALUES ?", [productRow], (err) => {
        if (err)
          return conn.rollback(() => {
            throw err;
          });

        conn.query(
          "INSERT INTO product_detail VALUES ?",
          [productDetailRow],
          (err, results, fields) => {
            if (err) {
              return conn.rollback(() => {
                throw err;
              });
            }
            conn.commit(callback);
          }
        );
      });
    });
  },
  
  delete: (id, callback) => {
    conn.beginTransaction((err) => {
      if (err) throw err;
      conn.query(
        `DELETE FROM product_detail WHERE idProduct = '${id}';`,
        (err) => {
          if (err)
            return conn.rollback(() => {
              throw err;
            });

          conn.query(`DELETE FROM products WHERE id = '${id}';`, (err) => {
            if (err)
              return conn.rollback(() => {
                throw err;
              });
            conn.commit(callback);
          });
        }
      );
    });
  },
};

module.exports = Product;
