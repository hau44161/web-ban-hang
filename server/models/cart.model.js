const conn = require("../mysql");

const Cart = {
  getAll: (idUser, callback) => {
    let sql = `SELECT * FROM product_in_cart where idUser = '${idUser}';`;
    conn.query(sql, callback);
  },
  delete: (idUser, idProduct, callback) => {
    let sql = `DELETE FROM product_in_cart WHERE idUser = '${idUser}' and idProduct = '${idProduct}';`;
    conn.query(sql, callback);
  },
  edit: (data, callback) => {
    let sql = `UPDATE product_in_cart SET ? where idUser = '${data.idUser}' and idProduct = '${data.idProduct}'`;
    conn.query(sql, data, callback);
  },
  add: (data, callback) => {
    //const sql = `REPLACE INTO product_in_cart VALUES ? `;
    const sql = `INSERT INTO product_in_cart VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE count=?;`

    conn.query(sql, data, callback);
  },
};

module.exports = Cart;
