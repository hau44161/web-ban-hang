const express = require("express");
const router = express.Router();


const cartController = require('../controllers/cart.controller.js');

router.get('/', cartController.getAll);
router.post('/', cartController.add);
router.delete('/:id', cartController.remove);
router.put('/', cartController.update);

module.exports = router;