const express = require("express");
const router = express.Router();

const auth = require("../middlewares/auth.middleware");
const userController = require("../controllers/user.controller.js");

router.post("/register", userController.add);
router.post("/remove/:id", auth.requireAuth, userController.remove);
router.post("/login", userController.login);
router.get("/logout", auth.requireAuth, userController.logout);

module.exports = router;
