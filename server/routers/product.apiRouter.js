const express = require("express");
const multer = require("multer");
const router = express.Router();

const upload = multer({ dest: "public/productImages" });
const productController = require("../controllers/product.controller.js");

router.get("/", productController.getAll);
router.get("/search", productController.search);
router.get("/flashSale", productController.getProductHaveFlashSale);
router.get("/:id", productController.getProductById);
router.get("/categorys/:id", productController.getProductByCategory);
router.get("/promotions/:id", productController.getProductByPromotion);
router.get("/productdetail/:id", productController.getProductDetailById);

router.post("/add", upload.single("imgSrc"), productController.add);

router.delete('/remove', productController.remove);

module.exports = router;
