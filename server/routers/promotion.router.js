const express = require("express");
const router = express.Router();

const promotionController = require("../controllers/promotion.controller");

router.get("/", promotionController.getAll);

//gui id vao query la ok vs ?id=...
router.get("/remove/:id", promotionController.remove);

//req nhan body theo thu tu la
//name percent money_sale freeship
router.post("/add", promotionController.add);
module.exports = router;