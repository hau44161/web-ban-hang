const express = require("express");
const router = express.Router();

const locationController = require('../controllers/location.controller')

router.get('/', locationController.getAll)
router.get('/add', locationController.add)
router.get('/locationByCode', locationController.getCode)
router.get("/remove/:id", locationController.remove);
router.get("/setDataDefaults", locationController.setDataDefaults);

module.exports = router;