require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
//const multer  = require('multer')
const http = require('http');
const socket = require('socket.io');
const cookieParser = require('cookie-parser');
const cors = require('cors');

//require file path
const promotionRouter = require('./routers/promotion.router');
const productRouter = require('./routers/product.router');
const locationRouter = require('./routers/location.router');
const userRouter = require('./routers/user.router');
const categoryRouter = require('./routers/category.router');
const productApiRouter = require('./routers/product.apiRouter');
const cartRouter = require('./routers/cart.router');

//require middlewares
const auth = require('./middlewares/auth.middleware.js');

//define
const app = express();
const io = socket(http.createServer(app));
const port = process.env.PORT || 2000;

io.on('connection', (socket) => {
    console.log('a user connected');
});

//const upload  = multer();

//set up
app.set('view engine', 'pug');
app.set('views', './views');

//middlewares
app.use(
    cors({
        origin: 'http://localhost:3000',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        allowedHeaders: 'Content-Type,Authentication',
    })
);
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(upload.array());
app.use(cookieParser(process.env.SESSION_SECRET));

//router
app.use('/promotions', auth.requireAuth, promotionRouter);
app.use('/products', productRouter);
app.use('/locations', locationRouter);

app.use('/api/cart', auth.requireAuth, cartRouter);
app.use('/api/users', userRouter);
app.use('/api/categorys', categoryRouter);
app.use('/api/products', productApiRouter);

app.get('/', (req, res) => {
    res.render('home');
});

//listening
app.listen(port, () => {
    //handler()
});

var handler = function () {
    server.close();
};
