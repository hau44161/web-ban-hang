import {
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_DETAIL_SUCCESS,
  GET_CATEGORY_SUCCESS,
  GET_KEYWORDS_SUCCESS,
  GET_PRODUCT_BY_ID_SUCCESS,
  GET_PRODUCT_FLASHDEAL_SUCCESS,
  LOGIN_SUCCESS,
  SIGNUP_SUCCESS,
  GET_CART_SUCCESS,
  ADD_CART_SUCCESS,
  DELETE_CART_SUCCESS,
  EDIT_CART_SUCCESS,
} from "./actionTypes";
import Category from "../../api/categoryAPI";
import Product from "../../api/productsAPI";
import User from "../../api/usersAPI";
import Cart from "../../api/cartAPI";

export function loadProductsSuccess(data) {
  return { type: GET_PRODUCTS_SUCCESS, data };
}
export function loadProductDetailSuccess(data) {
  return { type: GET_PRODUCT_DETAIL_SUCCESS, data };
}
export function loadProductFlashDealSuccess(data) {
  return { type: GET_PRODUCT_FLASHDEAL_SUCCESS, data };
}
export function loadCategorySuccess(data) {
  return { type: GET_CATEGORY_SUCCESS, data };
}
export function loadKeywordsSuccess(keywords) {
  return { type: GET_KEYWORDS_SUCCESS, keywords };
}

export function loginSuccess(data) {
  return { type: LOGIN_SUCCESS, data };
}

export function signUpSuccess(data) {
  return { type: SIGNUP_SUCCESS, data };
}
export function loadProductByIdSuccess(data) {
  return { type: GET_PRODUCT_BY_ID_SUCCESS, data };
}
export function loadCartSuccess(data) {
  return { type: GET_CART_SUCCESS, data };
}
export function addCartSuccess(data) {
  return { type: ADD_CART_SUCCESS, data };
}
export function deletedCartSuccess(id) {
  return { type: DELETE_CART_SUCCESS, id };
}
export function editCartSuccess(data) {
  return { type: EDIT_CART_SUCCESS, data };
}

// fetch url and dispatch action
export function login(data) {
  return function (dispatch) {
    return User.login(data)
      .then((data) => {
        dispatch(loginSuccess(data));
      })
      .catch((err) => {
        console.error("Loi dispatch loginSuccess");
        throw err;
      });
  };
}
export function signup(data) {
  return function (dispatch) {
    return User.signup(data)
      .then((data) => {
        dispatch(signUpSuccess(data));
      })
      .catch((err) => {
        console.error("Loi dispatch loginSuccess");
        throw err;
      });
  };
}
export function loadCategory() {
  return function (dispatch) {
    return Category.getCategory()
      .then((data) => {
        dispatch(loadCategorySuccess(data));
      })
      .catch((err) => {
        console.err("loi dispatch loadCategorySuccess");
        throw err;
      });
  };
}

//#region cart
export function loadCart() {
  return function (dispatch) {
    return Cart.getProductInCart().then((data) => {
      dispatch(loadCartSuccess(data));
    });
  };
}
export function addCart(data) {
  return function (dispatch) {
    return Cart.addProductInCart(data)
      .then((data) => {
        dispatch(addCartSuccess(data));
      })
      .catch((err) => {
        console.error("loi dispatch addCartSuccess");
        throw err;
      });
  };
}
export function editCart(data) {
  return function (dispatch) {
    return Cart.editProductByKeywords(data).then((res) => {
      dispatch(editCartSuccess(data));
    });
  };
}
export function removeCart(id) {
  return function (dispatch) {
    return Cart.deleteProductById(id).then((res) => {
      if (res.affectedRows !== 0) dispatch(deletedCartSuccess(id));
      console.error("cant delete product");
    });
  };
}
//#region

//#region product
export function loadProductById(id) {
  return function (dispatch) {
    return Product.getProductById(id)
      .then((data) => {
        dispatch(loadProductByIdSuccess(data));
      })
      .catch((err) => {
        console.error("loi dispatch loadProductByIdSuccess");
        throw err;
      });
  };
}
export function loadProduct() {
  return function (dispatch) {
    return Product.getProducts()
      .then((data) => {
        dispatch(loadProductsSuccess(data));
      })
      .catch((err) => {
        console.error("loi dispatch loadProductsSuccess");
        throw err;
      });
  };
}
export function loadProductFlashDeal() {
  return function (dispatch) {
    return Product.getProductsHaveFlashDeal()
      .then((data) => {
        dispatch(loadProductFlashDealSuccess(data));
      })
      .catch((err) => {
        console.error("loi dispatch loadProductsSuccess");
        throw err;
      });
  };
}
export function loadProductDetailById(id) {
  return function (dispatch) {
    return Product.getProductDetailById(id)
      .then((data) => {
        dispatch(loadProductDetailSuccess(data[0]));
      })
      .catch((err) => {
        console.error("loi dispatch loadProductDetailSuccess");
        throw err;
      });
  };
}
export function loadProductByCategory(id) {
  return function (dispatch) {
    return Product.getProductByCategory(id)
      .then((data) => {
        dispatch(loadProductsSuccess(data));
      })
      .catch((err) => {
        console.err("loi dispatch loadProductsSuccess");
        throw err;
      });
  };
}
export function loadProductBySearch(keywords) {
  return function (dispatch) {
    return Product.getProductByKeywords(keywords)
      .then((data) => {
        dispatch(loadProductsSuccess(data));
      })
      .catch((err) => {
        console.err("loi dispatch loadProductsSuccess");
        throw err;
      });
  };
}
//#region
