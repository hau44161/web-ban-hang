import { combineReducers } from "redux";
import products from "./reducers/productReducer";
import productDetail from "./reducers/productDetailReducer";
import categorys from "./reducers/categoryReducer";
import user from "./reducers/userReducer";
import productFlashDeal from "./reducers/productFlashDealReducers";
import cart from "./reducers/cartReducer";

const rootReducers = combineReducers({
  products,
  productFlashDeal,
  productDetail,
  categorys,
  user,
  cart,
});
export default rootReducers;
