import initState from "./initStates";
import { GET_PRODUCT_FLASHDEAL_SUCCESS } from "../actions/actionTypes";

export default function productDetailReducer(state = initState.productFlashDeal, action) {
  switch (action.type) {
    case GET_PRODUCT_FLASHDEAL_SUCCESS:
      return action.data;
    default:
      return state;
  }
}
