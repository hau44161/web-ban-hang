import initState from "./initStates";
import { GET_PRODUCT_DETAIL_SUCCESS } from "../actions/actionTypes";

export default function productDetailReducer(state = initState.productDetail, action) {
  switch (action.type) {
    case GET_PRODUCT_DETAIL_SUCCESS:
      return [...state, action.data];
    default:
      return state;
  }
}
