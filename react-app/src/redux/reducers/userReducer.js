import initState from "./initStates";
import { SIGNUP_SUCCESS, LOGIN_SUCCESS } from "../actions/actionTypes";

export default function productReducer(state = initState.user, action) {
  switch (action.type) {
    case SIGNUP_SUCCESS:
      return action.data;
    case LOGIN_SUCCESS:
      return action.data;
    default:
      return state;
  }
}
