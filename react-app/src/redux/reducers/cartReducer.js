import initState from "./initStates";
import {
  GET_CART_SUCCESS,
  ADD_CART_SUCCESS,
  EDIT_CART_SUCCESS,
  DELETE_CART_SUCCESS,
} from "../actions/actionTypes";

export default function cartReducer(state = initState.cart, action) {
  switch (action.type) {
    case ADD_CART_SUCCESS:
      let newState = state.filter((c) => c.idProduct !== action.data.idProduct);
      return [...newState, action.data];

    case DELETE_CART_SUCCESS:
      return state.filter((c) => c.idProduct !== action.id);

    case GET_CART_SUCCESS:
      return action.data;

    case EDIT_CART_SUCCESS:
      return state.map((c) =>
        c.idProduct === action.data.productId
          ? { ...c, count: action.data.count }
          : c
      );

    default:
      return state;
  }
}
