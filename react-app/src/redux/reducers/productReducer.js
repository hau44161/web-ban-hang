import initState from "./initStates";
import {
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_BY_ID_SUCCESS,
} from "../actions/actionTypes";

export default function productReducer(state = initState.products, action) {
  switch (action.type) {
    case GET_PRODUCTS_SUCCESS:
      return action.data;
    case GET_PRODUCT_BY_ID_SUCCESS: {
      let newState = [...state, ...action.data];
      return newState;
    }
    default:
      return state;
  }
}
