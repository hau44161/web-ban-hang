import initState from "./initStates";
import {
  GET_CATEGORY_SUCCESS,
} from "../actions/actionTypes";

export default function productDetailReducer(state = initState.categorys, action) {
  switch (action.type) {
    case GET_CATEGORY_SUCCESS:
      return action.data;
    default:
      return state;
  }
}
