import React from "react";
import { render } from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import configureStore from "./redux/configureStore";
import "./index.css";
import * as serviceWorker from "./serviceWorker";

render(
  <Provider store={configureStore()}>
    <App />
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
