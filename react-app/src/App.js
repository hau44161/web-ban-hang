import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./components/HomePage";
import './App.scss';

function App() {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route component={HomePage} path="/" />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
