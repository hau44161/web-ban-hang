import axiosClient from "./axiosClient";

const usersApi = {
  login: (data) => {
    const url = `/users/login`;
    return axiosClient.post(url, data);
  },
  signup: (data) => {
    const url = `/users/register`;
    return axiosClient.post(url, data);
  },
};
export default usersApi;
