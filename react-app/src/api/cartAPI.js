import axiosClient from "./axiosClient";

const cartApi = {
  getProductInCart: () => {
    const url = `/cart`;
    return axiosClient.get(url);
  },
  addProductInCart: (data) => {
    const url = `/cart`;
    return axiosClient.post(url, data);
  },
  deleteProductById: (id) => {
    const url = "/cart/" + id;
    return axiosClient.delete(url);
  },
  editProductByKeywords: (data) => {
    const url = "/cart";
    return axiosClient.put(url, data);
  },
};
export default cartApi;
