import axiosClient from "./axiosClient";

const productApi = {
  getProducts: (page=1) => {
    const url = `/products/?page=${page}`;
    return axiosClient.get(url);
  },
  getProductsHaveFlashDeal: () => {
    const url = `/products/flashSale`;
    return axiosClient.get(url);
  },
  getProductById: (id="") => {
    const url = "/products/"+id;
    return axiosClient.get(url);
  },
  getProductByKeywords: (keyword="") => {
    const url = "/products/search"+keyword;
    return axiosClient.get(url);
  },
  getProductDetailById: (id="") => {
    const url = `/products/productDetail/${id}`;
    return axiosClient.get(url);
  },
  getProductByCategory: (id="") => {
    const url = `/products/categorys/${id}`;
    return axiosClient.get(url);
  },
  getProductByPromotion: (id="") => {
    const url = `/products/promotions/${id}`;
    return axiosClient.get(url);
  }
};
export default productApi;
