import axios from "axios";

export default axios({
  method: "GET",
  url: "https://5f126b63d5e6c90016ee502e.mockapi.io/api/v1/products",
  data: null,
})
  .then((res) => JSON.parse(res))
  .then((res) => console.log(res))
  .catch((err) => console.error(err));
