import React, { useEffect, useState } from "react";

export const TimeDown = ({ hours, second = 0 }) => {
  const [time, setTime] = useState({ hours, second });

  useEffect(() => {
    let t = setInterval(() => {
      if (time.hours === 0 && time.second === 0) return;
      if (time.second === 0) setTime({ hours: time.hours - 1, second: 59 });
      else setTime({ hours: time.hours, second: time.second - 1 });
    }, 1000);
    return () => {
      clearInterval(t);
    };
  }, [time]);
  return (
    <>
      <span className="time-down">{time.hours < 10 ? `0${time.hours}` : time.hours}</span>:
      <span className="time-down">{time.second  < 10 ? `0${time.second}` : time.second}</span>
    </>
  );
};
