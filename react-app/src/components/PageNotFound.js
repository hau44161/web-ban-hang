import React from "react";
import "./PageNotFound.scss";

const PageNotFound = () => {
  return <div className="whole-page-not-found"><h2>404| Page not found</h2></div>;
};
export default PageNotFound;
