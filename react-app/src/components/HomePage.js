import React from "react";
import Narbar from "./Narbar";
import Footer from "./Footer";
import Home from "./Home";
import { Route, useLocation, Switch } from "react-router-dom";
import HomeSearch from "./HomeSearch";
import LockIn from "./LockIn";
import HomeProduct from "./HomeProduct";
import HomeCategory from "./HomeCategory";
import HomeCart from "./HomeCart";
import PrivateRoute from "./PrivateRoute";
const useQuery = () => {
  return new URLSearchParams(useLocation().search);
};

const HomePage = () => {
  let query = useQuery();
  
  return (
    <div style={{ backgroundColor: "whitesmoke" }}>
      <Narbar valueSearch={query.get("keyword")} />
      <Route exact path="/login">
        <LockIn page="Login" />
      </Route>
      <Route exact path="/signup">
        <LockIn page="Sign up" />
      </Route>
      <Switch>
        <PrivateRoute component={HomeCart} path="/shopping_cart"/>
        <Route path="/search" component={HomeSearch}/>
        <Route path="/products/categorys/:id" component={HomeCategory}/>
        <Route path="/products/:productId" component={HomeProduct}/>
        <Route exact path="/" component={Home} />
      </Switch>
      <Footer />
    </div>
  );
};

export default HomePage;