import React from "react";
import { connect } from "react-redux";
import { loadProductsSuccess } from "../redux/actions/actions";
import PropTypes from "prop-types";

const style = {
  btn: {
    border: "none",
    margin: "5px",
    display: "inline-block",
    borderRadius: "2px",
    lineHeight: "1em",
    minHeight: "1em",
    whiteSpace: "nowrap",
    textAlign: "center",
    cursor: "pointer",
    fontSize: "1rem",
    fontWeight: "700",
    padding: "0.75em 1em",
    background: "#E0E1E2",
    textDecoration: "none",
    fontFamily: "Lato",
    outline: "none",
    verticalAlign: "baseline",
    zoom: "1",
    userSelect: "none",
    transition: "all 0.1s linear",
    color: "#fff",
    backgroundColor: "#2185D0",
    "&:hover": {
      background: "#efefef",
    },
  },
  sortArea: {
    maxWidth: "1270px",
    minWidth: "fit-content",
    backgroundColor: "white",
    marginTop: "5px",
    borderBottom: "1px solid #f0f0f0",
  },
  title: {
    lineHeight: "70px",
    height: "70px",
    borderBottom: "1px solid #f0f0f0",
  },
};

const FilterCategory = ({ products, loadProductsSuccess: loadProduct }) => {
  const sortPriceSale = (a, b) => {
    return (
      -parseInt(a.priceSale.split(".").join("").slice(0, -1)) +
      parseInt(b.priceSale.split(".").join("").slice(0, -1))
    );
  };
  const sortPercentSale = (a, b) => {
    return -parseInt(a.sale.slice(1, -1)) + parseInt(b.sale.slice(1, -1));
  };
  const handleSort = (sort) => {
    let newState = [...products];
    newState.sort(sort);
    loadProduct(newState);
  };
  return (
    <div style={style.sortArea}>
      <div style={style.title}>
        <button style={style.btn} onClick={() => handleSort(sortPriceSale)}>
          Sort by price
        </button>
        <button style={style.btn} onClick={() => handleSort(sortPercentSale)}>
          Sort by Sale%
        </button>
      </div>
    </div>
  );
};
FilterCategory.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
  loadProductsSuccess: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  loadProductsSuccess,
};

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterCategory);
