import React, { useEffect, useState } from "react";
import { Header, Footer } from "./pages/HomePage";
import { Banner, Category } from "./pages/ProductsPage";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Spinner from "./Spinner";
import productApi from "../api/productsAPI";
import { loadProductsSuccess } from "../redux/actions/actions";
import FilterCategory from "./FilterCategory";

const style = {
  sortArea: {
    width: "1270px",
    backgroundColor: "white",
    paddingLeft: "20px",
    borderBottom: "1px solid #f0f0f0",
    lineHeight: "70px",
  },
  title: {
    height: "70px",
    borderBottom: "1px solid #f0f0f0",
  },
};

function ManageProductsPage({ products, loadProductsSuccess: loadProduct }) {
  const [origin, setOrigin] = useState([]);
  const [isExist, setIsExist] = useState(true);
  useEffect(() => {
    if (!products[0]) fetchProducts();
    if (!origin[0]) setOrigin(products);
  }, [products]);
  const fetchProducts = async () => {
    try {
      const res = await productApi.getProducts();
      loadProduct(res);
    } catch (error) {
      console.error("Failed to fetch.", error);
    }
  };
  const handleSearch = (keyWord) => {
    let kq = origin.filter((c) => c.title.toLowerCase().includes(keyWord));
    if (kq.length === 0) setIsExist(false);
    else setIsExist(true);

    loadProduct(kq);
  };
  return (
    <>
      <Header handleSearch={handleSearch} />
      <Banner />
      <FilterCategory />
      {isExist ? (
        ""
      ) : (
        <div className="sort-area" style={style.sortArea}>
          <div className="deal-title" style={style.title}>
            <h2>Tim khong thay</h2>
          </div>
        </div>
      )}
      {products ? <Category products={products} /> : <Spinner />}
      <Footer />
    </>
  );
}

ManageProductsPage.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
  loadProductsSuccess: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  loadProductsSuccess,
};

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageProductsPage);
