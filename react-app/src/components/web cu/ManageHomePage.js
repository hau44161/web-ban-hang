import React, { useEffect } from "react";
import Spinner from "./Spinner";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import productApi from "../api/productsAPI";
import {
  Header,
  Banner,
  DealProducts,
  CareProducts,
  HotProducts,
  ForYouProducts,
  Footer,
} from "./pages/HomePage";
import {
  loadProductsSuccess,
  //loadProductDetailSuccess,
  loadImageCategorySuccess,
  loadKeywordsSuccess,
} from "../redux/actions/actions";

function ManageHomePage({
  carePro,
  keywords,
  products,
  loadProductsSuccess: loadProduct,
  loadImageCategorySuccess: loadImg,
  loadKeywordsSuccess: loadKey,
}) {
  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const res = await productApi.getProducts();
        loadProduct(res);
      } catch (error) {
        console.error("Failed to fetch.", error);
      }
    };
    const fetchImg = async () => {
      try {
        const res1 = await productApi.getImageCategory();
        await loadImg(res1);
      } catch (error) {
        console.error("Failed to fetch.", error);
      }
    };
    const fetchKey = async () => {
      try {
        const res = await productApi.getKeywords();
        await loadKey(res);
      } catch (error) {
        console.error("Failed to fetch.", error);
      }
    };
    fetchProducts();
    fetchImg();
    fetchKey();
  }, []);
  
  return (
    <>
      <Header />
      <Banner />
      {products[0] ? <DealProducts products={products} /> : <Spinner />}
      {carePro[0] ? <CareProducts products={carePro} /> : <Spinner />}
      {keywords[0] ? <HotProducts keywords={keywords} /> : <Spinner />}
      {products[0] ? <ForYouProducts products={products} /> : <Spinner />}
      <Footer />
    </>
  );
}

ManageHomePage.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
  carePro: PropTypes.arrayOf(Object).isRequired,
  keywords: PropTypes.arrayOf(Object).isRequired,
  loadProductsSuccess: PropTypes.func.isRequired,
  loadImageCategorySuccess: PropTypes.func.isRequired,
  loadKeywordsSuccess: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  loadProductsSuccess,
  loadKeywordsSuccess,
  loadImageCategorySuccess,
};

const mapStateToProps = (state) => ({
  products: state.products,
  carePro: state.orther.imgCategory,
  keywords: state.orther.keywords,
});

export default connect(mapStateToProps, mapDispatchToProps)(ManageHomePage);
