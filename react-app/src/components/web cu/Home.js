import React from "react";
//import logo from "../images/logo/Asset115px.png";

const Home = () => {
  return (
    <>
      <div className="container-fluid ">
        <div className="row .bg-light d-flex align-content-end ">
          <div className="col col-lg-2">
            <a className="navbar-brand flex-grow-2" href="#">
              FOR YOU
            </a>
          </div>
          <div className="col col-lg-6 d-flex ">
            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Nhập từ cần tìm..."
                aria-label="Recipient's username"
                aria-describedby="button-addon2"
              />
              <div className="input-group-append">
                <button
                  className="btn btn-outline-secondary px-lg-5"
                  type="button"
                  id="button-addon2"
                >
                  Tìm
                </button>
              </div>
            </div>
          </div>
          <div className="col col-lg-3">
            <ul className=" d-flex w-100">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Thông báo <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Giỏ hàng
                </a>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Tài khoản
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a className="dropdown-item" href="#">
                    Thông tin cá nhân
                  </a>
                  <a className="dropdown-item" href="#">
                    Địa chỉ
                  </a>
                  <a className="dropdown-item" href="#">
                    Đơn hàng
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <nav className="navbar  navbar-expand-lg navbar-light bg-light mb-3">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
        </nav>
      </div>
    </>
  );
};

export default Home;
