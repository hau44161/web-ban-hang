import React, { useEffect } from "react";
import { Header, Footer } from "./pages/HomePage";
import {
  Product,
  ProductDetail,
  ProductQA,
  ProductComment,
} from "./pages/ProductPage";
import {
  loadProductDetailSuccess,
  loadProductsSuccess,
} from "../redux/actions/actions";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Spinner from "./Spinner";
import productApi from "../api/productsAPI";

function ManageProductPage({
  product,
  productDetail,
  loadProductDetailSuccess: loadDetail,
  loadProductsSuccess: loadProduct,
}) {
  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const res = await productApi.getProducts();
        loadProduct(res);
      } catch (error) {
        console.error("Failed to fetch.", error);
      }
    };
    const fetchProductDetail = async () => {
      try {
        const res = await productApi.getProductDetailById(product.id);
        loadDetail(res);
      } catch (error) {
        console.error("Failed to fetch.", error);
      }
    };
    fetchProducts();
    if (product.id)
      fetchProductDetail();
  }, [product]);
  return (
    <>
      <Header />
      {product && productDetail.imgSrc ? (
        <Product product={product} productDetail={productDetail} />
      ) : (
        <Spinner />
      )}
      {productDetail.star ? (
        <ProductDetail detail={productDetail.detail} />
      ) : (
        <Spinner />
      )}
      {productDetail.star ? <ProductQA QA={productDetail.QA} /> : <Spinner />}
      <ProductComment />
      <Footer />
    </>
  );
}
ManageProductPage.propTypes = {
  productDetail: PropTypes.object.isRequired,
  product: PropTypes.object.isRequired,
  loadProductsSuccess: PropTypes.func.isRequired,
  loadProductDetailSuccess: PropTypes.func.isRequired,
};

const getIdFromSlugg = (products = [], slugg) =>
  products.find((c) => c.slugg === slugg) || null;

const mapDispatchToProps = {
  loadProductsSuccess,
  loadProductDetailSuccess,
};

const mapStateToProps = (state, ownProps) => {
  const slugg = ownProps.match.params.slugg;
  let product =
    slugg && state.products.length > 0
      ? getIdFromSlugg(state.products, slugg)
      : {};
  return {
    product,
    products: state.products, //co can hay kkhong
    productDetail: state.productDetail,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageProductPage);
