import React from "react";
import PropTypes from "prop-types";
import "../../css/products.css";
import "../../css/style.css";

export const Product = ({ product, productDetail }) => {
  const star = () => {
    let arr = [1, 2, 3, 4, 5];
    return arr.map((c, index) =>
      c <= productDetail.star ? (
        <li key={index}>
          <i className="fas fa-star"></i>
        </li>
      ) : (
        <li key={index} style={{ color: "white" }}>
          <i className="fas fa-star"></i>
        </li>
      )
    );
  };
  return (
    <div className="xem-sp">
      <div className="xem-hinh">
        <ul className="ds-hinh-anh">
          {productDetail.imgSrc.map((c, index) => (
            <li key={index}>
              <img src={c} alt="" />
            </li>
          ))}
        </ul>
        <div className="hinh-chinh">
          <img
            src="https://salt.tikicdn.com/cache/w80/ts/product/ca/b5/90/6c79d4052478d66ecf166a42f673b33a.jpg"
            alt=""
          />
        </div>
      </div>
      <div className="thong-tin-co-ban">
        <h2 className="product-name">{product.title}</h2>
        <ul className="danh-gia mobile-hidden tablet-hidden">
          {star()}
          <li className="so-nhan-xet tablet-hidden">
            (xem {productDetail.comment.length} nhận xét)
          </li>
        </ul>
        <h3 className="product-price-sale">{product.priceSale}</h3>
        <p className="product-price">{product.price}</p>
        <ul className="uu-diem-sp">
          {productDetail.describer.map((c, index) => (
            <li key={index}>{c}</li>
          ))}
        </ul>
        <div className="chon-mau-sac">
          <span>Chọn màu sắc: </span>
          <ul>
            {productDetail.color.map((c, index) => (
              <li key={index}>{c}</li>
            ))}
          </ul>
        </div>
        <div className="chon-so-luong">
          <p>Số lượng:</p>
          <div className="group-btn-so-luong">
            <p>-</p>
            <p>1</p>
            <p>+</p>
          </div>
          <div className="btn-mua">
            <i className="fas fa-shopping-cart"></i>
            Chọn mua
          </div>
        </div>
      </div>
    </div>
  );
};
export const ProductDetail = ({ detail }) => {
  return (
    <div className="thong-tin-chi-tiet-sp">
      <h3 className="danh-rieng-title">Thông tin chi tiết</h3>
      <table>
        <tbody>
          {detail.map((c, index) => (
            <tr key={index}>
              <td>{c.title}</td>
              <td>{c.value}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
export const ProductQA = (props) => {
  return (
    <div className="hoi-dap-sp">
      <h3 className="danh-rieng-title">Hỏi đáp về sản phẩm</h3>
      {props.QA.map((c, index) => (
        <div key={index}>
          <p className="cau-hoi">{c.question}</p>
          <AnswerList anwser={c.anwser} />
        </div>
      ))}
      <input
        className="nhap-cau-tra-loi"
        type="text"
        placeholder="Nhập câu hỏi..."
      />
      <button className="btn-gui-cau-hoi mobile-hidden">Gửi câu hỏi</button>
      <button className="btn-gui-cau-hoi mobile-show tablet-show">Gửi</button>
    </div>
  );
};
const AnswerList = (props) =>
  props.anwser.map((d, index1) => (
    <div key={index1}>
      <p className="cau-tra-loi">{d.content}</p>
      <p className="da-tra-loi">
        {d.who} trả lời vào {d.date}
      </p>
      <button className="btn-like btn">
        <i className="fas fa-thumbs-up"></i>
        {d.like} Thích
      </button>
      <button className="btn-reply btn">
        <i className="fas fa-reply"></i> Trả lời
      </button>
    </div>
  ));
export const ProductComment = () => {
  return (
    <div className="nhan-xet-sp">
      <h3 className="danh-rieng-title">Khách hàng nhận xét</h3>
      <ul className="nhan-xet">
        <li className="nhan-xet-contain">CHỨC NĂNG CHƯA HOÀN THÀNH</li>
      </ul>
    </div>
  );
};

Product.propTypes = {
  product: PropTypes.object.isRequired,
  productDetail: PropTypes.object.isRequired,
};
ProductDetail.propTypes = {
  detail: PropTypes.arrayOf(Object).isRequired,
};
ProductQA.propTypes = {
  QA: PropTypes.arrayOf(Object).isRequired,
};
