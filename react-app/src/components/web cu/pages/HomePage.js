import React, { useState } from "react";
import PropTypes from "prop-types";
import "../../css/style.css";
import { Link } from "react-router-dom";

export const Header = ({ handleSearch }) => {
  const [value, setValue] = useState("");
  return (
    <>
      <header>
        <div className="header-row1 #top">
          <div className="header-contain">
            <div className="logo header-contain-item">
              <a href="/">
                <h2>For You</h2>
              </a>
            </div>
            <div className="input header-contain-item mobile-hidden">
              <input
                type="text"
                value={value}
                onChange={(e) => setValue(e.target.value)}
                placeholder="Search..."
              />
              <button
                className="tablet-hidden"
                type="button"
                onClick={() => handleSearch(value.toLowerCase())}
              >
                Tim kiem
              </button>
              <div className="search-icon tablet-show">
                <i className="fas fa-search"></i>
              </div>
            </div>
            <div className="thong-bao header-contain-item">
              <a href="#no">
                <i className="fas fa-bell"></i>
                <span className="tablet-hidden mobile-hidden">Thong bao</span>
              </a>
            </div>
            <div className="gio-hang header-contain-item">
              <a href="#no">
                <i className="fas fa-shopping-cart"></i>
                <span className="tablet-hidden mobile-hidden">Gio hang</span>
              </a>
            </div>
            <div className="tai-khoan header-contain-item">
              <i className="fas fa-user"></i>
              <span className="tablet-hidden mobile-hidden">Nguoi dung</span>
            </div>
          </div>
        </div>
        <div className="header-row2">
          <div className="header-contain">
            <div className="input-mobile mobile-show">
              <input
                type="text"
                placeholder="Search..."
                value={value}
                onChange={(e) => setValue(e.target.value)}
              />
              <div
                className="search-icon tablet-show"
                onClick={() => handleSearch(value.toLowerCase())}
              >
                <i className="fas fa-search"></i>
              </div>
            </div>
            <div className="danh-muc tablet-hidden mobile-hidden">
              <i className="fas fa-bars"></i>
              <span className="tablet-hidden">DANH MUC SAN PHAM</span>
            </div>
            <div className="sp-moi-xem mobile-hidden">
              <span>San pham moi xem: </span>
              <ul>
                <li>
                  <a href="#">
                    <img
                      src="https://salt.tikicdn.com/cache/280x280/media/catalog/product/k/h/khi-hoi-tho-hoa-thinh-khong.u5464.d20170726.t170655.288851.jpg"
                      alt="1"
                    />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export const Banner = () => (
  <>
    <div className="danh-muc-qc">
      <div className="danh-muc-qc-contain">
        <div className="danh-muc-sp">
          <ul className="danh-muc-sp-contain">
            <li>
              <a href="./products.html">
                <i className="fas fa-tshirt tablet-hidden mobile-hidden"></i>
                Quần
              </a>
              <ul className="quan-nam-content lv2">
                <li>
                  <a href="#">Quần jean</a>
                  <ul className="quan-jean lv3">
                    <li>
                      <a href="#">Quần jean rách</a>
                    </li>
                    <li>
                      <a href="#">Quần jean ống đứng</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#">Quần kaki</a>
                </li>
                <li>
                  <a href="#">Quần tây</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
                Áo
              </a>
              <ul className="ao-nam-content lv2">
                <li>
                  <a href="#">Áo sơ mi</a>
                  <ul className="ao-so-mi lv3">
                    <li>
                      <a href="#ao-so-mi-mau">Áo sơ mi màu</a>
                    </li>
                    <li>
                      <a href="#ao-so-mi-caro">Áo sơ mi caro</a>
                    </li>
                    <li>
                      <a href="#ao-so-mi-hoa-tiet">Áo sơ mi họa tiết</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#">Áo thun</a>
                  <ul className="ao-thun lv3">
                    <li>
                      <a href="#">Áo thun cổ tròn</a>
                    </li>
                    <li>
                      <a href="#">Áo thun cổ tim</a>
                    </li>
                    <li>
                      <a href="#">Áo thun tay dài</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#">Áo khoác</a>
                  <ul className="ao-khoac lv3">
                    <li>
                      <a href="#">Áo khoác dù</a>
                    </li>
                    <li>
                      <a href="#">Áo khoác vải</a>
                    </li>
                    <li>
                      <a href="#">Áo khoác nỉ</a>
                    </li>
                    <li>
                      <a href="#">Áo khoác jean</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-shoe-prints tablet-hidden mobile-hidden"></i>
                Dày
              </a>
              <ul className="giay-nam-content lv2">
                <li>
                  <a href="#">Dày thể thao</a>
                </li>
                <li>
                  <a href="#">Dày tây</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
                Dép
              </a>
              <ul className="phu-kien-content lv2">
                <li>
                  <a href="#">Ví</a>
                </li>
                <li>
                  <a href="#">Dây nịch</a>
                </li>
                <li>
                  <a href="#">Mũ</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
                Mũ
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
                Guốc
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
                Dây nịch
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-clock tablet-hidden mobile-hidden"></i>Nước
                hoa
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
                Balo
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-wallet tablet-hidden mobile-hidden"></i>Ví
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-clock tablet-hidden mobile-hidden"></i>Đồng
                hồ
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-shopping-bag tablet-hidden mobile-hidden"></i>
                Túi sách
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fas fa-shopping-bag tablet-hidden mobile-hidden"></i>
                Khăn choàng
              </a>
            </li>
          </ul>
        </div>
        <div className="qc">
          <ul className="qc-contain">
            <li className="qc-hot-product">
              <a href="#">
                <img
                  src="https://salt.tikicdn.com/cache/w584/ts/banner/47/44/6a/0239ef03ad58e74703b229d46cc67747.jpg"
                  alt=""
                />
              </a>
            </li>
            <li className="qc-product">
              <a href="#">
                <img
                  src="https://salt.tikicdn.com/cache/w206/ts/banner/9d/10/76/f15d0670bbe4e4c166777223ba7ea708.jpg"
                  alt=""
                />
              </a>
            </li>
            <li className="qc-product">
              <a href="#">
                <img
                  src="https://salt.tikicdn.com/cache/w206/ts/banner/48/bf/72/dbb51248bea59e90370622390d84345f.jpg"
                  alt=""
                />
              </a>
            </li>
            <li className="qc-product mobile-hidden">
              <a href="#">
                <img
                  src="https://salt.tikicdn.com/cache/w206/ts/banner/3b/b7/2f/eab6019d349454c0c6b5102a48503e32.jpg"
                  alt=""
                />
              </a>
            </li>
            <li className="qc-product tablet-hidden mobile-hidden">
              <a href="#">
                <img
                  src="https://salt.tikicdn.com/cache/w206/ts/banner/8e/8e/62/2e285ca734e27337e0720dc21f116cb5.jpg"
                  alt=""
                />
              </a>
            </li>
            <li className="qc-product tablet-hidden mobile-hidden">
              <a href="#">
                <img
                  src="https://salt.tikicdn.com/cache/w206/ts/banner/27/90/54/382ec063f7830cf68f0246cdc8712af5.jpg"
                  alt=""
                />
              </a>
            </li>
            <li className="qc-product tablet-hidden mobile-hidden">
              <a href="#">
                <img
                  src="https://salt.tikicdn.com/cache/w206/ts/banner/1d/78/ab/011d4bdad974c4602f2b0b6bbd33b425.jpg"
                  alt=""
                />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </>
);
export const pro = (c, index) => (
  <li className="product" key={index}>
    {
      <Link to={"./product/" + c.slugg}>
        <span className="sale">{c.sale}</span>
        <img src={c.imgSrc} />
        <h5 className="product-name">{c.title}</h5>
        <span className="product-price-sale">{c.priceSale}</span>
        <span className="product-price tablet-hidden mobile-hidden">
          {c.price}
        </span>
      </Link>
    }
  </li>
);

const pros = (products = [], title = "", detail = "") => (
  <div className="for-you-deal">
    <div className="deal-title">
      <h3>{title}</h3>
      <p className="mobile-hidden">{detail}</p>
    </div>
    <div className="for-you-deal-contain">
      <ul className="deal-product">
        {products.map((c, index) => (index < 8 ? pro(c, index) : ""))}
        {products[8] ? pro(products[8], 7) : ""}
        {products[9] ? pro(products[9], 8) : ""}
      </ul>
    </div>
    <div className="xem-them">
      <a href="./products/">Xem thêm</a>
    </div>
  </div>
);
export const DealProducts = ({ products }) =>
  pros(
    products,
    "For You deal",
    "Còn chần chờ gì nữa hãy mau chọn cho mình những sản phẩm phù hợp đi nào!!!"
  );

export const CareProducts = ({ products }) => (
  <>
    <div className="muc-quan-tam">
      <div className="muc-quan-tam-contain">
        <h3 className="muc-quan-tam-title">Ngành hàng quan tâm</h3>
        <ul className="ds-quan-tam">
          {products.map((c, index) => (
            <li key={index}>
              <a href="#">
                <img src={c.imgSrc} />
                <p>{c.nameCategory}</p>
              </a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  </>
);

export const HotProducts = ({ keywords }) => (
  <>
    <div className="tu-khoa-hot">
      <div className="tu-khoa-hot-contain">
        <h3 className="tu-khoa-hot-title">Từ khóa hot</h3>
        <ul className="ds-tu-khoa">
          {keywords.map((c, index) => (
            <li key={index}>
              <a href="#">{c.word}</a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  </>
);

export const ForYouProducts = ({ products }) =>
  pros(products, "Dành riêng cho bạn", "Just for you ^^");

export const Footer = () => (
  <>
    <footer>
      <div className="footer-contain">
        <div className="gioi-thieu">
          <ul className="gioi-thieu-contain">
            <li className="gioi-thieu-lv2">
              <p className="gioi-thieu-title">HỖ TRỢ KHÁCH HÀNG</p>
              <p>Hotline: 0383793662</p>
              <p>Làm việc tất cả các ngày</p>
              <ul className="gioi-thieu-lv3">
                <li>
                  <a href="#">Các câu hỏi thường gặp</a>
                </li>
                <li>
                  <a href="#">Gửi yêu cầu hỗ trợ</a>
                </li>
                <li>
                  <a href="#">Chính sách đổi trả</a>
                </li>
                <li>
                  <a href="#">Hướng dẫn mua trả góp</a>
                </li>
              </ul>
            </li>
            <li className="gioi-thieu-lv2">
              <p className="gioi-thieu-title">VỀ FOR YOU</p>
              <ul className="gioi-thieu-lv3">
                <li>
                  <a href="#">Giới thiệu</a>
                </li>
                <li>
                  <a href="#">Tuyển dụng</a>
                </li>
                <li>
                  <a href="#">Chính sách khiếu nại</a>
                </li>
                <li>
                  <a href="#">Điều khoản sử dụng</a>
                </li>
                <li>
                  <a href="#">Chính sách bảo mật thông tin cá nhân</a>
                </li>
              </ul>
            </li>
            <li className="gioi-thieu-lv2 ">
              <p className="gioi-thieu-title">KẾT NỐI VỚI CHÚNG TÔI</p>
              <ul className="gioi-thieu-lv3 icon-app">
                <li>
                  <a href="#">
                    <i className="fab fa-facebook"></i>Facebook
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fab fa-youtube"></i>Youtube
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fab fa-twitter"></i>Twitter
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div className="dia-chi">
          <h3>Địa chỉ shop: </h3>
          <ul className="dia-chi-contain">
            <li>
              <span>
                {" "}
                Cơ sở 1: 52 Út Tịch, phường 4, quận Tân Bình, TP Hồ Chí Minh
              </span>
            </li>
            <li>
              <span>
                {" "}
                Cơ sở 2: 14/43 Lê Thị Hồng, phường 17, quận Gò Vấp, TP Hồ Chí
                Minh
              </span>
            </li>
            <li>
              <span>
                {" "}
                Cơ sở 3: 477/45 Nguyễn Văn Công, phường 3, quận Gò Vấp, TP Hồ
                Chí Minh
              </span>
            </li>
          </ul>
        </div>
        <div className="ban-quyen mobile-hidden">
          <p>
            &copy; 2016 - Bản quyền của Công Ty một thành viên For You - fory.vn
          </p>
          <span className="tablet-hidden">
            Giấy chứng nhận Đăng ký Kinh doanh số 0309532909 do Sở Kế hoạch và
            Đầu tư Thành phố Hồ Chí Minh cấp ngày 06/06/2020
          </span>
        </div>
        <p className="mobile-show ban-quyen">
          &copy; 2016 - Bản quyền thuộc For You
        </p>
      </div>
    </footer>
  </>
);
Header.propTypes = {
  handleSearch: PropTypes.func,
};
DealProducts.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
};
CareProducts.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
};
ForYouProducts.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
};
HotProducts.propTypes = {
  keywords: PropTypes.arrayOf(Object).isRequired,
};
