import React from "react";
import PropTypes from "prop-types";
//import { Link } from "react-router-dom";
import "../../css/products.css";
import { pro } from "./HomePage";

export const Banner = () => (
  <div className="danh-muc-qc">
    <div className="danh-muc-qc-contain">
      <div className="danh-muc-sp">
        <ul className="danh-muc-sp-contain">
          <li>
            <a href="#">
              <i className="fas fa-tshirt tablet-hidden mobile-hidden"></i>Quần
            </a>
            <ul className="quan-nam-content lv2">
              <li>
                <a href="#">Quần jean</a>
                <ul className="quan-jean lv3">
                  <li>
                    <a href="#">Quần jean rách</a>
                  </li>
                  <li>
                    <a href="#">Quần jean ống đứng</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">Quần kaki</a>
              </li>
              <li>
                <a href="#">Quần tây</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
              Áo
            </a>
            <ul className="ao-nam-content lv2">
              <li>
                <a href="#">Áo sơ mi</a>
                <ul className="ao-so-mi lv3">
                  <li>
                    <a href="#ao-so-mi-mau">Áo sơ mi màu</a>
                  </li>
                  <li>
                    <a href="#ao-so-mi-caro">Áo sơ mi caro</a>
                  </li>
                  <li>
                    <a href="#ao-so-mi-hoa-tiet">Áo sơ mi họa tiết</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">Áo thun</a>
                <ul className="ao-thun lv3">
                  <li>
                    <a href="#">Áo thun cổ tròn</a>
                  </li>
                  <li>
                    <a href="#">Áo thun cổ tim</a>
                  </li>
                  <li>
                    <a href="#">Áo thun tay dài</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">Áo khoác</a>
                <ul className="ao-khoac lv3">
                  <li>
                    <a href="#">Áo khoác dù</a>
                  </li>
                  <li>
                    <a href="#">Áo khoác vải</a>
                  </li>
                  <li>
                    <a href="#">Áo khoác nỉ</a>
                  </li>
                  <li>
                    <a href="#">Áo khoác jean</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-shoe-prints tablet-hidden mobile-hidden"></i>
              Dày
            </a>
            <ul className="giay-nam-content lv2">
              <li>
                <a href="#">Dày thể thao</a>
              </li>
              <li>
                <a href="#">Dày tây</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
              Dép
            </a>
            <ul className="phu-kien-content lv2">
              <li>
                <a href="#">Ví</a>
              </li>
              <li>
                <a href="#">Dây nịch</a>
              </li>
              <li>
                <a href="#">Mũ</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
              Mũ
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
              Guốc
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
              Dây nịch
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-clock tablet-hidden mobile-hidden"></i>Nước
              hoa
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-hat-cowboy tablet-hidden mobile-hidden"></i>
              Balo
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-wallet tablet-hidden mobile-hidden"></i>Ví
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-clock tablet-hidden mobile-hidden"></i>Đồng
              hồ
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-shopping-bag tablet-hidden mobile-hidden"></i>
              Túi sách
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fas fa-shopping-bag tablet-hidden mobile-hidden"></i>
              Khăn choàng
            </a>
          </li>
        </ul>
      </div>
      <div className="qc">
        <ul className="qc-contain">
          <li className="qc-danh-muc">
            <a href="#">
              <img
                style={{ width: "100vw" }}
                src="https://salt.tikicdn.com/cache/w885/ts/banner/32/2a/e6/c67a053081355280bf292a06af59c5f0.jpg"
                alt=""
              />
            </a>
          </li>
          <li className="qc-danh-muc">
            <a href="#">
              <img
                style={{ width: "100vw" }}
                src="https://salt.tikicdn.com/desktop/img/category-default-banner.png"
                alt=""
              />
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
);
export const Category = ({ products }) => {
  return (
    <>
      <div className="danh-rieng">
        <div className="danh-rieng-contain">
          <h3 className="danh-rieng-title">Danh sách sản phẩm theo loại</h3>
          <ul className="ds-danh-rieng">
            {products.map((c, index) => (index < 60 ? pro(c, index) : ""))}
          </ul>
          <div className="xem-them">
            <a href="./products/abc">Xem thêm</a>
          </div>
        </div>
      </div>
    </>
  );
};
Category.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
};
