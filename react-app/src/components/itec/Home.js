import React from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { Narbar } from "./Narbar";
import ContentHome from "./ContentHome";
import { Sidebar } from "./Sidebar";
import { ContentActivitis } from "./ContentActivitis";
import "./Home.scss";

const Home = () => {
  return (
    <>
      <Narbar />
      <div className="maxheight maxwidth" style={{ margin: "0 auto" }}>
        <div className="ui grid maxheight">
          <div className="row ">
            <div className="four wide computer column">
              <Sidebar />
            </div>
            <div className="twelve wide computer column">
              <div className="ui segment maxheight scrollauto">
                <Switch>
                  <Route path="/" exact component={ContentHome} />
                  <Route path="/activitis:slugg" component={ContentActivitis} />
                </Switch>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default withRouter(Home);
