import React from "react";
import { Link } from "react-router-dom";

export const Sidebar = () => {
  return (
    <>
      <div className="ui fluid vertical big menu maxheight">
        <div className="item">
          <div className="header">Activitis</div>
          <div className="menu">
            <Link to="/activitis/:2020" className="item">
              The year 2020
            </Link>
            <Link to="/" className="item">
              The year 2019
            </Link>
          </div>
        </div>
        <div className="item">
          <div className="header">About ITEC</div>
          <div className="menu">
            <Link to="/" className="item">
              Member
            </Link>
            <Link to="/" className="item">
              Leaders
            </Link>
          </div>
        </div>
        <div className="item">
          <div className="header">Hosting</div>
          <div className="menu">
            <Link to="/" className="item">
              Shared
            </Link>
            <Link to="/" className="item">
              Dedicated
            </Link>
          </div>
        </div>
        <div className="item">
          <div className="header">Contact</div>
          <div className="menu">
            <Link to="/" className="item">
              E-mail Support
            </Link>
            <Link to="/" className="item">
              FAQs
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};
