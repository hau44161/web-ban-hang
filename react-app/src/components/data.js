
const products = [
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/media/catalog/product/k/h/khi-hoi-tho-hoa-thinh-khong.u5464.d20170726.t170655.288851.jpg",
    title: "Khi Hơi Thở Hóa Thinh Không",
    price: "109.000 ₫",
    sale: "-38%",
    priceSale: "68.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ba/80/a3/3eee5b22e9f3963da9c9517caf342ed3.jpg",
    title: "Máy Rửa Bát Electrolux ESF6010BW - Hàng chính hãng",
    price: "10.990.000 ₫",
    sale: "-36%",
    priceSale: "7.099.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/94/7f/9f/cfeab3032cc192d8d1f8f4b467c381d3.jpg",
    title:
      "Bình Giữ Nhiệt Thép Không Gỉ Vacuum Bottle Lock&Lock LHC6180FU (800ml) - Xanh Đậm",
    price: "587.000 ₫",
    sale: "-40%",
    priceSale: "357.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/26/e2/3a/7a4a20de944afa7a83430ea4ba4a33e8.jpg",
    title: "Tã Quần Pampers Giữ Dáng Mới Gói Cực Đại M74 (74 Miếng)",
    price: "399.000 ₫",
    sale: "-29%",
    priceSale: "286.900 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/c8/5d/47/d160ccde6d523a4e36c19c835c3c4595.jpg",
    title: "Bộ Dây Ngũ Sắc Tập Thể Hình Đa năng",
    price: "590.000 ₫",
    sale: "-80%",
    priceSale: "118.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ad/19/31/3ea7f3b12a772025147aaf92b0790a17.jpg",
    title:
      "Máy Xay Sinh Tố Nagakawa NAG0806 (300W - 1.25 Lít) - Hàng Chính Hãng",
    price: "500.000 ₫",
    sale: "-47%",
    priceSale: "269.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/24/cc/7f/49a8772b42f32f3bfb737d470f87b0ab.jpg",
    title: "Ăn Dặm Không Phải Là Cuộc Chiến (Tái Bản)",
    price: "219.000 ₫",
    sale: "-44%",
    priceSale: "124.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ae/87/95/944b918d36c86748028273671f656431.jpg",
    title: "Bếp Gas Hồng Ngoại Soho 899HG - Hàng chính hãng",
    price: "590.000 ₫",
    sale: "-51%",
    priceSale: "291.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ab/88/8f/7108ff9ca71e73de4c9f9e9f5252dad2.jpg",
    title: "Ổ cắm điện NAKAGAMI 8 lỗ dây dài 3m",
    price: "170.000 ₫",
    sale: "-39%",
    priceSale: "104.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/f9/da/08/b5a3388e46f6569d0bee97719788348d.jpg",
    title: "Máy Làm Tỏi Đen TIROSS TS904 - Hàng chính hãng",
    price: "2.420.000 ₫",
    sale: "-59%",
    priceSale: "999.000 ₫",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/cb/e2/06/c818dd07d700a442119008ffa60e026a.jpg",
    title: "Điện Thoại OPPO A3s (16GB/2GB) - Hàng Chính Hãng",
    sale: "3.690.000 ₫",
    priceSale: "2.550.000 ₫-31%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/bc/23/09/76d09086ceaa3d0d9905fe56644e9e9e.jpg",
    title: "Điện Thoại Xiaomi Mi 8 Lite (4GB / 64GB) - Hàng Chính Hãng",
    sale: "6.690.000 ₫",
    priceSale: "3.190.000 ₫-53%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/e7/f9/79/d204695a3bf602ff5b2afb7b1731f1ef.jpg",
    title: "Điện Thoại iPhone XR 64GB - Hàng Nhập Khẩu Chính Hãng",
    sale: "17.990.000 ₫",
    priceSale: "14.990.000 ₫-17%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/3e/59/92/255085a2b9fe9c285cde56eea1d2c6c6.jpg",
    title: "iPad Pro 11 inch (2018) 64GB Wifi - Hàng Nhập Khẩu Chính Hãng",
    sale: "22.990.000 ₫",
    priceSale: "18.990.000 ₫-18%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/2b/83/af/a0af003b498d3aa871b4b9791f87b0e4.jpg",
    title:
      "Nồi Chiên Không Dầu Lock&Lock EJF351BLK (5.2L) (Đen) - Hàng chính hãng",
    sale: "4.700.000 ₫",
    priceSale: "2.450.000 ₫-48%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/e8/53/42/449e60a01e388f0c5343e438ba91c978.jpg",
    title: "Tã Quần Huggies Dry Gói Cực Đại XXL56 (56 Miếng) - Bao Bì Mới",
    sale: "390.000 ₫",
    priceSale: "308.000 ₫-22%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/32/b2/d6/91eb335efb59729acadc3bfe260c584b.jpg",
    title: "Điện Thoại iPhone XS Max 256GB - Nhập Khẩu Chính Hãng",
    sale: "30.990.000 ₫",
    priceSale: "23.990.000 ₫-23%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/2b/a3/7f/fd733b7695f36b01abe0198b52fc0745.jpg",
    title:
      "Combo 3 Gói Tã Quần Huggies Dry Gói Cực Đại XL62 (62 Miếng) - Bao Bì Mới",
    sale: "1.170.000 ₫",
    priceSale: "909.000 ₫-23%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ee/70/5c/2b59d7f9ae2638ae4fe26f2cd81c47f6.jpg",
    title: "Nồi Chiên Không Dầu Philips HD9220/20 - Hàng chính hãng",
    sale: "4.999.000 ₫",
    priceSale: "2.928.000 ₫-42%",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/f5/32/3f/70fe686b158b83af76bc466e44815064.jpg",
    title: "Tã Dán Huggies Dry Gói Cực Đại L68 (68 Miếng) - Bao Bì Mới",
    sale: "320.000 ₫",
    priceSale: "257.000 ₫-20%",
  },
];


export const products = [
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/media/catalog/product/k/h/khi-hoi-tho-hoa-thinh-khong.u5464.d20170726.t170655.288851.jpg",
    title: "Khi Hơi Thở Hóa Thinh Không",
    price: "109.000 ₫",
    sale: "-38%",
    priceSale: "68.000 ₫",
    slugg: "khi-hoi-tho-hoa-thinh-khong",
    category: "Điện Gia Dụng",
    id: 1,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ba/80/a3/3eee5b22e9f3963da9c9517caf342ed3.jpg",
    title: "Máy Rửa Bát Electrolux ESF6010BW - Hàng chính hãng",
    price: "10.990.000 ₫",
    sale: "-36%",
    priceSale: "7.099.000 ₫",
    category: "Điện Gia Dụng",
    slugg: "may-rua-bat-electrolux-esf6010bw-hang-chinh-hang",
    id: 2,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/94/7f/9f/cfeab3032cc192d8d1f8f4b467c381d3.jpg",
    title:
      "Bình Giữ Nhiệt Thép Không Gỉ Vacuum Bottle Lock&Lock LHC6180FU (800ml) - Xanh Đậm",
    price: "587.000 ₫",
    sale: "-40%",
    priceSale: "357.000 ₫",
    category: "Điện Gia Dụng",
    slugg:
      "binh-giu-nhiet-thep-khong-gi-vacuum-bottle-lock-lock-lhc6180fu-800ml-xanh-dam",
    id: 3,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/26/e2/3a/7a4a20de944afa7a83430ea4ba4a33e8.jpg",
    title: "Tã Quần Pampers Giữ Dáng Mới Gói Cực Đại M74 (74 Miếng)",
    price: "399.000 ₫",
    sale: "-29%",
    priceSale: "286.900 ₫",
    category:"Đồ Chơi - Mẹ & Bé",
    slugg: "ta-quan-pampers-giu-dang-moi-goi-cuc-dai-m74-74-mieng",
    id: 4,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/c8/5d/47/d160ccde6d523a4e36c19c835c3c4595.jpg",
    title: "Bộ Dây Ngũ Sắc Tập Thể Hình Đa năng",
    price: "590.000 ₫",
    sale: "-80%",
    priceSale: "118.000 ₫",
    category: "Điện Gia Dụng",
    slugg: "bo-day-ngu-sac-tap-the-hinh-da-nang",
    id: 5,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ad/19/31/3ea7f3b12a772025147aaf92b0790a17.jpg",
    title:
      "Máy Xay Sinh Tố Nagakawa NAG0806 (300W - 1.25 Lít) - Hàng Chính Hãng",
    price: "500.000 ₫",
    sale: "-47%",
    priceSale: "269.000 ₫",
    category: "Điện Gia Dụng",
    slugg: "may-xay-sinh-to-nagakawa-nag0806-300w-1-25-lit-hang-chinh-hang",
    id: 6,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/24/cc/7f/49a8772b42f32f3bfb737d470f87b0ab.jpg",
    title: "Ăn Dặm Không Phải Là Cuộc Chiến (Tái Bản)",
    price: "219.000 ₫",
    sale: "-44%",
    priceSale: "124.000 ₫",
    category: "Đồ Chơi - Mẹ & Bé",
    slugg: "an-dam-khong-phai-la-cuoc-chien-tai-ban",
    id: 7,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ae/87/95/944b918d36c86748028273671f656431.jpg",
    title: "Bếp Gas Hồng Ngoại Soho 899HG - Hàng chính hãng",
    price: "590.000 ₫",
    sale: "-51%",
    priceSale: "291.000 ₫",
    category: "Điện Gia Dụng",
    slugg: "bep-gas-hong-ngoai-soho-899hg-hang-chinh-hang",
    id: 8,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ab/88/8f/7108ff9ca71e73de4c9f9e9f5252dad2.jpg",
    title: "Ổ cắm điện NAKAGAMI 8 lỗ dây dài 3m",
    price: "170.000 ₫",
    sale: "-39%",
    priceSale: "104.000 ₫",
    category: "Điện Gia Dụng",
    slugg: "o-cam-dien-nakagami-8-lo-day-dai-3m",
    id: 9,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/f9/da/08/b5a3388e46f6569d0bee97719788348d.jpg",
    title: "Máy Làm Tỏi Đen TIROSS TS904 - Hàng chính hãng",
    price: "2.420.000 ₫",
    sale: "-59%",
    priceSale: "999.000 ₫",
    category: "Điện Gia Dụng",
    slugg: "may-lam-toi-den-tiross-ts904-hang-chinh-hang",
    id: 10,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/cb/e2/06/c818dd07d700a442119008ffa60e026a.jpg",
    title: "Điện Thoại OPPO A3s (16GB/2GB) - Hàng Chính Hãng",
    sale: "3.690.000 ₫",
    priceSale: "2.550.000 ₫-31%",
    category: "Điện Thoại - Máy Tính Bảng",
    slugg: "dien-thoai-oppo-a3s-16gb-2gb-hang-chinh-hang",
    id: 11,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/bc/23/09/76d09086ceaa3d0d9905fe56644e9e9e.jpg",
    title: "Điện Thoại Xiaomi Mi 8 Lite (4GB / 64GB) - Hàng Chính Hãng",
    sale: "6.690.000 ₫",
    priceSale: "3.190.000 ₫-53%",
    category: "Điện Thoại - Máy Tính Bảng",
    slugg: "dien-thoai-xiaomi-mi-8-lite-4gb-64gb-hang-chinh-hang",
    id: 12,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/e7/f9/79/d204695a3bf602ff5b2afb7b1731f1ef.jpg",
    title: "Điện Thoại iPhone XR 64GB - Hàng Nhập Khẩu Chính Hãng",
    sale: "17.990.000 ₫",
    priceSale: "14.990.000 ₫-17%",
    category: "Điện Thoại - Máy Tính Bảng",
    slugg: "dien-thoai-iphone-xr-64gb-hang-nhap-khau-chinh-hang",
    id: 13,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/3e/59/92/255085a2b9fe9c285cde56eea1d2c6c6.jpg",
    title: "iPad Pro 11 inch (2018) 64GB Wifi - Hàng Nhập Khẩu Chính Hãng",
    sale: "22.990.000 ₫",
    priceSale: "18.990.000 ₫-18%",
    category: "Điện Thoại - Máy Tính Bảng",
    slugg: "ipad-pro-11-inch-2018-64gb-wifi-hang-nhap-khau-chinh-hang",
    id: 14,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/2b/83/af/a0af003b498d3aa871b4b9791f87b0e4.jpg",
    title:
      "Nồi Chiên Không Dầu Lock&Lock EJF351BLK (5.2L) (Đen) - Hàng chính hãng",
    sale: "4.700.000 ₫",
    priceSale: "2.450.000 ₫-48%",
    category: "Điện Gia Dụng",
    slugg: "noi-chien-khong-dau-lock-lock-ejf351blk-5-2l-den-hang-chinh-hang",
    id: 15,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/e8/53/42/449e60a01e388f0c5343e438ba91c978.jpg",
    title: "Tã Quần Huggies Dry Gói Cực Đại XXL56 (56 Miếng) - Bao Bì Mới",
    sale: "390.000 ₫",
    priceSale: "308.000 ₫-22%",
    category: "Đồ Chơi - Mẹ & Bé",
    slugg: "ta-quan-huggies-dry-goi-cuc-dai-xxl56-56-mieng-bao-bi-moi",
    id: 16,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/32/b2/d6/91eb335efb59729acadc3bfe260c584b.jpg",
    title: "Điện Thoại iPhone XS Max 256GB - Nhập Khẩu Chính Hãng",
    sale: "30.990.000 ₫",
    priceSale: "23.990.000 ₫-23%",
    category: "Điện Thoại - Máy Tính Bảng",
    slugg: "dien-thoai-iphone-xs-max-256gb-nhap-khau-chinh-hang",
    id: 17,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/2b/a3/7f/fd733b7695f36b01abe0198b52fc0745.jpg",
    title:
      "Combo 3 Gói Tã Quần Huggies Dry Gói Cực Đại XL62 (62 Miếng) - Bao Bì Mới",
    sale: "1.170.000 ₫",
    priceSale: "909.000 ₫-23%",
    category: "Đồ Chơi - Mẹ & Bé",
    slugg:
      "combo-3-goi-ta-quan-huggies-dry-goi-cuc-dai-xl62-62-mieng-bao-bi-moi",
    id: 18,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ee/70/5c/2b59d7f9ae2638ae4fe26f2cd81c47f6.jpg",
    title: "Nồi Chiên Không Dầu Philips HD9220/20 - Hàng chính hãng",
    sale: "4.999.000 ₫",
    priceSale: "2.928.000 ₫-42%",
    category: "Điện Gia Dụng",
    slugg: "noi-chien-khong-dau-philips-hd9220-20-hang-chinh-hang",
    id: 19,
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/f5/32/3f/70fe686b158b83af76bc466e44815064.jpg",
    title: "Tã Dán Huggies Dry Gói Cực Đại L68 (68 Miếng) - Bao Bì Mới",
    sale: "320.000 ₫",
    priceSale: "257.000 ₫-20%",
    category: "Đồ Chơi - Mẹ & Bé",
    slugg: "ta-dan-huggies-dry-goi-cuc-dai-l68-68-mieng-bao-bi-moi",
    id: 20,
  },
];

export const carePro = [
  {
    href: "#",
    imgSrc:
      "https://salt.tikicdn.com/ts/category/a6/9f/45/460fdecbbe0f81da09c7da37aa08f680.png",
    nameCategory: "Thực Phẩm Tươi Sống",
  },
  {
    href: "#",
    imgSrc:
      "https://salt.tikicdn.com/ts/category/66/15/4f/6282e8c6655cb87cb226e3b701bb9137.png",
    nameCategory: "Đồ Chơi - Mẹ & Bé",
  },
  {
    href: "#",
    imgSrc:
      "https://salt.tikicdn.com/ts/category/93/27/e3/192b0ebe1d4658c51f9931bda62489b2.png",
    nameCategory: "Điện Thoại - Máy Tính Bảng",
  },
  {
    href: "#",
    imgSrc:
      "https://salt.tikicdn.com/ts/category/85/13/02/d8e5cd75fd88862d0f5f647e054b2205.png",
    nameCategory: "Làm Đẹp - Sức Khỏe",
  },
  {
    href: "#",
    imgSrc:
      "https://salt.tikicdn.com/ts/category/b3/2b/72/8e7b4b703653050ffc79efc8ee017bd0.png",
    nameCategory: "Điện Gia Dụng",
  },
  {
    href: "#",
    imgSrc:
      "https://salt.tikicdn.com/ts/category/dd/51/92/e6bc22b5ec0d6d965a93f056b7776493.png",
    nameCategory: "Thời Trang",
  },
];

export const keywords = [
  { word: "tai nghe bluetooth" },
  { word: "đồng hồ thông minh" },
  { word: "balo" },
  { word: "giày nữ" },
  { word: "quạt mini" },
  { word: "bình giữ nhiệt" },
  { word: "tinh dầu" },
  { word: "sim 4g" },
  { word: "lock & lock" },
  { word: "vợt muỗi" },
  { word: "điện thoại oppo" },
  { word: "tai nghe" },
];
//<Link to={"/course/" + course.slug}>{course.title}</Link>
export const productDetail = {
  imgSrc: [
    "https://salt.tikicdn.com/cache/w80/ts/product/ca/b5/90/6c79d4052478d66ecf166a42f673b33a.jpg",
    "https://salt.tikicdn.com/cache/w80/ts/product/e0/98/bf/094fd92ff315893e17f7b2690c968032.jpg",
  ],
  describer: [
    "Tai nghe có Âm Bass Điện Tử đầu tiên tại Việt Nam",
    "Bass mạnh hơn gấp 3 lần với công nghệ Dynamic Bass",
    "Thời gian sử dụng lên đến 36 giờ",
    "Bluetooth 5.0 tiên tiến, mượt mà ổn định",
    "Thiết kế cá tính, phá cách, phù hợp cho những bạn trẻ năng động",
    "Thương hiệu âm thanh cao cấp Soul đến từ Mỹ",
    "Sản phẩm chính hãng Soul",
  ],
  color: ["Trắng", "Đen", "Xanh"],
  star: 4,
  detail: [
    { title: "Thương hiệu", value: "Soul" },
    { title: "Xuất xứ thương hiệu", value: "USA" },
    { title: "Kích thước", value: "10x8x4" },
    { title: "Model", value: "SU34" },
    { title: "Hỗ trợ đàm thoại	", value: "yes" },
    { title: "Bluetooth", value: "5m" },
    { title: "Thời gian pin", value: "36h" },
    { title: "SKU", value: "7699764336345" },
  ],
  QA: [
    {
      who: "Be Heo",
      question: "Cho em hỏi có được đi kèm jack 3.5mm ko ạ?",
      anwser: [
        {
          who: "Nguyen A",
          content:
            "Chào bạn, sản phẩm này không có jack 3.5 mm bạn nhé. Bạn vui lòng tham khảo thêm thông tin trên website và đặt hàng theo hướng dẫn https://hotro.tiki.vn/hc/vi/articles/203807174 hoặc liên hệ hotline 19006035 để đặt hàng nha. Cảm ơn bạn đã quan tâm :v",
          date: "7/6/2020",
          like: 2,
        },
      ],
    },
  ],
  comment: [],
};

[
  {
    "id": "1",
    "imgSrc": [
      "https://salt.tikicdn.com/cache/w80/ts/product/ca/b5/90/6c79d4052478d66ecf166a42f673b33a.jpg",
      "https://salt.tikicdn.com/cache/w80/ts/product/e0/98/bf/094fd92ff315893e17f7b2690c968032.jpg"
    ],
    "describer": [
      "Tai nghe có Âm Bass Điện Tử đầu tiên tại Việt Nam",
      "Bass mạnh hơn gấp 3 lần với công nghệ Dynamic Bass",
      "Thời gian sử dụng lên đến 36 giờ",
      "Bluetooth 5.0 tiên tiến, mượt mà ổn định",
      "Thiết kế cá tính, phá cách, phù hợp cho những bạn trẻ năng động",
      "Thương hiệu âm thanh cao cấp Soul đến từ Mỹ",
      "Sản phẩm chính hãng Soul"
    ],
    "color": [
      "Trắng",
      "Đen",
      "Xanh"
    ],
    "star": 4,
    "detail": [
      {
        "title": "Thương hiệu",
        "value": "Soul"
      },
      {
        "title": "Xuất xứ thương hiệu",
        "value": "USA"
      },
      {
        "title": "Kích thước",
        "value": "10x8x4"
      },
      {
        "title": "Model",
        "value": "SU34"
      },
      {
        "title": "Hỗ trợ đàm thoại\t",
        "value": "yes"
      },
      {
        "title": "Bluetooth",
        "value": "5m"
      },
      {
        "title": "Thời gian pin",
        "value": "36h"
      },
      {
        "title": "SKU",
        "value": "7699764336345"
      }
    ],
    "QA": [
      {
        "who": "Be Heo",
        "question": "Cho em hỏi có được đi kèm jack 3.5mm ko ạ?",
        "anwser": [
          {
            "who": "Nguyen A",
            "content": "Chào bạn, sản phẩm này không có jack 3.5 mm bạn nhé. Bạn vui lòng tham khảo thêm thông tin trên website và đặt hàng theo hướng dẫn https://hotro.tiki.vn/hc/vi/articles/203807174 hoặc liên hệ hotline 19006035 để đặt hàng nha. Cảm ơn bạn đã quan tâm :v",
            "date": "7/6/2020",
            "like": 2
          }
        ]
      }
    ],
    "comment": []
  },
  {
    "id": "2",
    "imgSrc": [
      "https://salt.tikicdn.com/cache/w80/ts/product/bc/23/09/76d09086ceaa3d0d9905fe56644e9e9e.jpg",
      "https://salt.tikicdn.com/cache/w80/ts/product/19/f0/11/fadf75ee03e1876d4f8d35fc5abebee8.jpg"
    ],
    "describer": [
      "Sản phẩm Chính hãng, Mới 100%, Nguyên seal, Chưa Active",
      "Miễn phí giao hàng toàn quốc",
      "Thiết kế: Nguyên khối",
      "Bluetooth 5.0 tiên tiến, mượt mà ổn định",
      "Thiết kế cá tính, phá cách, phù hợp cho những bạn trẻ năng động",
      "Thương hiệu âm thanh cao cấp Soul đến từ Mỹ",
      "Sản phẩm chính hãng Soul"
    ],
    "color": [
      "Trắng",
      "Đen"
    ],
    "star": 4,
    "detail": [
      {
        "title": "Thương hiệu",
        "value": "Xiaomi"
      },
      {
        "title": "Xuất xứ thương hiệu",
        "value": "TQ"
      },
      {
        "title": "Kích thước",
        "value": "6.2inch"
      },
      {
        "title": "Model",
        "value": "SU34"
      },
      {
        "title": "Hỗ trợ đàm thoại\t",
        "value": "yes"
      },
      {
        "title": "Bluetooth",
        "value": "5m"
      },
      {
        "title": "Thời gian pin",
        "value": "36h"
      },
      {
        "title": "SKU",
        "value": "7699764336345"
      }
    ],
    "QA": [
      {
        "who": "Be Heo",
        "question": "Cho em hỏi có được đi kèm jack 3.5mm ko ạ?",
        "anwser": [
          {
            "who": "Nguyen A",
            "content": "Chào bạn, sản phẩm này không có jack 3.5 mm bạn nhé. Bạn vui lòng tham khảo thêm thông tin trên website và đặt hàng theo hướng dẫn https://hotro.tiki.vn/hc/vi/articles/203807174 hoặc liên hệ hotline 19006035 để đặt hàng nha. Cảm ơn bạn đã quan tâm :v",
            "date": "7/6/2020",
            "like": 2
          }
        ]
      }
    ],
    "comment": []
  }
]