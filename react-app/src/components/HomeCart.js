import React, { useEffect } from "react";
import "./HomeCart.scss";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  editCart,
  loadCart,
  removeCart,
  loadProductById,
} from "../redux/actions/actions";
import { Link } from "react-router-dom";

const HomeCart = ({
  loadProductById,
  cart,
  products,
  editCart,
  loadCart,
  removeCart,
}) => {
  useEffect(() => {
    if (cart?.length === 0) loadCart();
    if (cart?.length) {
      let listIdProducts = products.map((c) => c.id);
      let list = cart.filter((c) => !listIdProducts.includes(c.idProduct));

      list.forEach((c) => {
        loadProductById(c.idProduct);
      });
    }
  }, [cart]);

  const handleCount = (id, count) => {
    console.log(count);
    editCart({ productId: id, count });
  };

  const handleDelete = (id) => {
    removeCart(id);
  };

  return (
    <>
      <div className="maxwidth">
        <div className="head-table">
          <ul className="head-row">
            <li>
              <input type="checkbox" name="" value="" />
              Sản phẩm
            </li>
            <li>Đơn giá</li>
            <li>Số lượng</li>
            <li>Số tiền</li>
            <li>Thao tác</li>
          </ul>
        </div>
        <div className="body-table">
          {cart &&
            cart
              .map((c) => ({
                ...products.find((i) => i.id === c.idProduct && i),
                ...c,
              }))
              .map((c, index) => (
                <ul className="row" key={index}>
                  <li>
                    {" "}
                    <div className="wrap-sp">
                      <input type="checkbox" name="" value="" />
                      <Link to={"products/" + c.id} className="flex">
                        {c?.imgSrc && <img src={c.imgSrc} alt="" />}
                        <p>{c.name}</p>
                      </Link>
                    </div>
                  </li>
                  <li>
                    <p>{c?.price && (c.price + "000").replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
                  </li>
                  <li>
                    <button
                      onClick={() =>
                        c.count > 1 && handleCount(c.id, c.count - 1)
                      }
                    >
                      -
                    </button>
                    <p>{c.count}</p>
                    <button onClick={() => handleCount(c.id, c.count -1 + 2)}>
                      +
                    </button>
                  </li>
                  <li>{c?.price && (c.price * c.count + "000").replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</li>
                  <li>
                    <button onClick={() => handleDelete(c.id)}>Xóa</button>
                  </li>
                </ul>
              ))}
        </div>
      </div>
    </>
  );
};
HomeCart.propTypes = {
  loadCart: PropTypes.func.isRequired,
  editCart: PropTypes.func.isRequired,
  removeCart: PropTypes.func.isRequired,
  cart: PropTypes.arrayOf(Object),
  products: PropTypes.arrayOf(Object),
};

const mapStateToProps = (state) => {
  return {
    products: state.products,
    cart: state.cart,
  };
};
const mapDispatchToProps = {
  editCart,
  loadCart,
  removeCart,
  loadProductById,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeCart);
