import React, { useState } from "react";
//import PropTypes from "prop-types";
import "./FilterProduct.scss";

const FilterProduct = () => {
  const [vision, setVision] = useState("")
  return (
    <>
      <button className="btn-loc" onClick={()=>setVision("visible")} >Loc</button>
      <div className="filter-product" style={{visibility:vision}}>
        <h4 className="text-center">BỘ LỌC TÌM KIẾM</h4>
        <ul>
          <li>Danh mục</li>
          <li>
            <label>
              <input type="radio" value="male" name="danh-muc" />
              Áo khoác
            </label>
          </li>
          <li>
            <label>
              <input type="radio" value="male" name="danh-muc" />
              Áo lông
            </label>{" "}
          </li>
          <li>
            <label>
              <input type="radio" value="male" name="danh-muc" />
              Áo tay
            </label>
          </li>
        </ul>
        <ul>
          <li>Nơi bán</li>
          <li>Hà Nội</li>
          <li>Tp HCM</li>
          <li>Đà Nẵng</li>
          <li>Vinh</li>
          <li>Huế</li>
        </ul>
        <ul className="khoang-gia">
          <li>Khoảng giá</li>
          <li>
            <input type="text" placeholder="Từ" />
            <input type="text" placeholder="Đến" />
          </li>
        </ul>
        <ul>
          <li>Đánh giá</li>
          <li>5 sao trở lên</li>
          <li>4 sao trở lên</li>
          <li>3 sao trở lên</li>
          <li>2 sao trở lên</li>
          <li>1 sao trở lên</li>
        </ul>
        <ul>
          <li>Khuyến mãi</li>
          <li>Free ship</li>
          <li>Đang giảm giá</li>
        </ul>
        <button onClick={()=>setVision("hidden")} >Xóa bộ lọc</button>
      </div>
      <div className="listen-click-out-side" style={{visibility: vision}} onClick={()=>setVision("hidden")}></div>
    </>
  );
};

FilterProduct.propTypes = {};

export default FilterProduct;
