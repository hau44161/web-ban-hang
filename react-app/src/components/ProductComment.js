import React from "react";
import './ProductComment.scss'

const ProductComment = () => {
  return (
    <div className="nhan-xet-sp">
      <h3 className="danh-rieng-title">Khách hàng nhận xét</h3>
      <ul className="nhan-xet">
        <li className="nhan-xet-contain">CHỨC NĂNG CHƯA HOÀN THÀNH</li>
      </ul>
    </div>
  );
};

export default ProductComment;
