import React from "react";
import "./LockIn.scss";
import PropTypes from "prop-types";
import { login, signup } from "../redux/actions/actions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

class LockIn extends React.Component {
  state = {
    email: "",
    password: "",
    phone_number: "",
    name: "",
    sex: "",
    err: "",
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  // {
  //   name: "Ngo Van Hau",
  //   password: "ahihi",
  //   email: "1851050043@gmail.com",
  //   sex: "Nam",
  // }
  submit = async (e) => {
    e.preventDefault();
    if (this.props.page === "Sign up")
      await this.props
        .signup(this.state)
        .then(() => {
          this.props.history.goBack(-1);
        })
        .catch((err) => {
          this.setState({ err: "err" });
        });
    else await this.props.login(this.state);
    this.props.history.goBack();
  };
  render() {
    const { email, password, phone_number, name, sex, err } = this.state;
    const { page } = this.props;
    const { handleChange, submit } = this;

    return (
      <>
        <div className="whole-lock-in">
          <div className="container-lock-in">
            <form onSubmit={submit}>
              <div
                className="btn-close"
                onClick={() => {
                  this.props.history.go(-1);
                }}
              >
                X
              </div>
              <h2 className="text-center">{page}</h2>

              {page === "Sign up" && (
                <div>
                  <input
                    value={name}
                    onChange={handleChange}
                    type="text"
                    name="name"
                    placeholder="Nhập user"
                  />
                </div>
              )}
              <div>
                <input
                  value={email}
                  onChange={handleChange}
                  type="text"
                  name="email"
                  placeholder="Nhập email"
                />
              </div>
              <div>
                <input
                  value={password}
                  onChange={handleChange}
                  type="password"
                  name="password"
                  placeholder="Nhập password"
                />
              </div>
              {page === "Sign up" && (
                <>
                  <div>
                    <input
                      value={phone_number}
                      onChange={handleChange}
                      type="text"
                      name="phone_number"
                      placeholder="Nhập số điện thoại"
                    />
                  </div>
                  <div>
                    <input
                      value={sex}
                      onChange={handleChange}
                      type="text"
                      name="sex"
                      placeholder="Nhập giới tính"
                    />
                  </div>
                </>
              )}
              <div>
                <p className="text-right tooltip">
                  quên mật khẩu??
                  <span className="tooltiptext">Chức năng chưa làm ^^</span>
                </p>
              </div>
              <p>{err}</p>
              <div>
                <button>{page === "login" ? "Log in" : "Sign up"}</button>
              </div>
            </form>
          </div>
        </div>
      </>
    );
  }
}

const mapDispatchToProps = { login, signup };

LockIn.propTypes = {
  page: PropTypes.string,
};

export default withRouter(connect(null, mapDispatchToProps)(LockIn));
