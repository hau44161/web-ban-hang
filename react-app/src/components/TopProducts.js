import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "./TopProducts.scss";

const TopProducts = ({ title, topProducts = [] }) => {
  return (
    <div className="maxwidth top-products">
      <h2>{title}</h2>
      <div className="container-top-product flex-wrap">
        {topProducts.map((c, index) => (
          <Link key={index} className="flash-item" to={"/products/" + (index+1)}>
            <p className="sale-percent">TOP 1</p>
            <div
              className="wrap-img"
              style={{
                backgroundImage: `url(${c.imgSrc})`,
              }}
            ></div>
            <p className="product-name">{c.title}</p>
          </Link>
        ))}
      </div>
    </div>
  );
};

TopProducts.propTypes = {
  title: PropTypes.string.isRequired,
  topProducts: PropTypes.arrayOf(Object),
  imgSrc: PropTypes.string,
};

export default React.memo(TopProducts);
