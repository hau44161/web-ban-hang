import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./ListCategory.scss";
import { loadCategory } from "../redux/actions/actions";

const ListCategory = ({ title = "Loại sản phẩm", categorys, loadCategory }) => {
  useEffect(() => {
    if (categorys.length === 0) {
      loadCategory().catch((err) => {});
    }
  }, []);

  return (
    <>
      <div className="maxwidth list-category">
        <h2>{title}</h2>
        <div className="container-item-category flex-wrap">
          {categorys.map((c, index) => (
            <Link
              key={index}
              className="category-item"
              to={`/products/categorys/${c.id}`}
            >
              <img src={c.imgSrc} alt="" />
              <p className="name-category">{c.category}</p>
            </Link>
          ))}
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    categorys: state.categorys,
  };
};

const mapDispatchToPro = {
  loadCategory,
};

ListCategory.propTypes = {
  loadCategory: PropTypes.func,
  title: PropTypes.string,
  id: PropTypes.string,
  imgSrc: PropTypes.string,
  category: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToPro)(ListCategory);
