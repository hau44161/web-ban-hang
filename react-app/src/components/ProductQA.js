import React from 'react'
import './ProductQA.scss'

const ProductQA = ({QA=[]}) => {
  return (
    <div>
      <div className="hoi-dap-sp">
      <h3 className="danh-rieng-title">Hỏi đáp về sản phẩm</h3>
      {QA.map((c, index) => (
        <div key={index}>
          <p className="cau-hoi">{c.question}</p>
          <AnswerList anwser={c.anwser} />
        </div>
      ))}
      <input
        className="nhap-cau-tra-loi"
        type="text"
        placeholder="Nhập câu hỏi..."
      />
      <button className="btn-gui-cau-hoi mobile-hidden">Gửi câu hỏi</button>
      <button className="btn-gui-cau-hoi mobile-show tablet-show">Gửi</button>
    </div>
    </div>
  )
}

const AnswerList = (props) =>
  props.anwser.map((d, index1) => (
    <div key={index1}>
      <p className="cau-tra-loi">{d.content}</p>
      <p className="da-tra-loi">
        {d.who} trả lời vào {d.date}
      </p>
      <button className="btn-like btn">
        <i className="fas fa-thumbs-up"></i>
        {d.like} Thích
      </button>
      <button className="btn-reply btn">
        <i className="fas fa-reply"></i> Trả lời
      </button>
    </div>
  ));

export default ProductQA;