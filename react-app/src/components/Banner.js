import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import oneImg from "../images/banners/one.jpeg";
import Background from "../images/banners/two.png";
import "./Banner.scss";

const Banner = ({ image = [] }) => {
  return (
    <div className="whole-banner">
      <div className="maxwidth">
        <div className="container-banner flex">
          <div className="image-banner image-one">
            <Link to="/">
              <img src={oneImg} alt="anh banner" />
            </Link>
          </div>
          <div className="image-banner image-two">
            <Link to="/">
              <img src={Background} alt="anh banner" />
            </Link>
            <Link to="/">
              <img src={oneImg} alt="anh banner" />
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

Banner.propTypes = {
  image: PropTypes.arrayOf(String),
};

export default React.memo(Banner);
