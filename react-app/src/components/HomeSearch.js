import React, { useEffect } from "react";
import "./HomeSearch.scss";
import FilterProduct from "./FilterProduct";
import SortProduct from "./SortProduct";
import ListProducts from "./ListProducts";
import { useLocation } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from 'prop-types'
import { loadProductBySearch } from "../redux/actions/actions";

const HomeSearch = ({loadProductBySearch, products}) => {
  const location = useLocation();
  useEffect(() => {
    loadProductBySearch(location.search).catch((err) => {
      console.error(err);
    });
  },[location.search]);
  return (
    <div className="whole-home-search">
      <div className="container-home-search maxwidth">
        <div className="flex-1">
          <FilterProduct />
        </div>
        <div className="flex-5">
          <SortProduct />
          <ListProducts title="Kết quả tìm kiếm" products={products} />
        </div>
      </div>
    </div>
  );
};

HomeSearch.propTypes = {
  loadProductBySearch: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(Object)
};

const mapDispatchToPro = {
  loadProductBySearch,
};

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};

export default connect(mapStateToProps, mapDispatchToPro)(HomeSearch);

