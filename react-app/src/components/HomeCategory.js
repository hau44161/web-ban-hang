import React, { useEffect } from "react";
import "./HomeSearch.scss";
import FilterProduct from "./FilterProduct";
import SortProduct from "./SortProduct";
import ListProducts from "./ListProducts";
import { loadProductByCategory, loadCategory } from "../redux/actions/actions";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const HomeCategory = ({
  loadProductByCategory,
  loadCategory,
  products,
  categorys,
}) => {
  const { id } = useParams();
  useEffect(() => {
    loadProductByCategory(id).catch((err) => {
      console.error(err);
    });
    if (categorys.length === 0) {
      loadCategory().catch((err) => {});
    }
  }, []);
  return (
    <div className="whole-home-search">
      <div className="container-home-search maxwidth">
        <div className="flex-1">
          <FilterProduct />
        </div>
        <div className="flex-5">
          <SortProduct />
          {categorys.length && 
            <ListProducts
              title={categorys.filter((c) => c.id === id)[0].category}
              products={products}
            />
          }
        </div>
      </div>
    </div>
  );
};

HomeCategory.propTypes = {
  loadProductByCategory: PropTypes.func,
  loadCategory: PropTypes.func,
  product: PropTypes.arrayOf(Object),
  categorys: PropTypes.arrayOf(Object),
};

const mapDispatchToProps = {
  loadProductByCategory,
  loadCategory,
};
const mapStateToProps = (state) => {
  return {
    products: state.products,
    categorys: state.categorys,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeCategory);
