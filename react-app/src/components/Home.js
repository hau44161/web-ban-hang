import React, { useEffect } from "react";
import Banner from "./Banner";
import ListCategory from "./ListCategory";
import FlashDeals from "./FlashDeals";
import ListProducts from "./ListProducts";
import TopProducts from "./TopProducts";
import { loadProduct } from "../redux/actions/actions";
import { connect } from "react-redux";

const Home = ({ loadProduct, products }) => {
  useEffect(() => {
    loadProduct().catch((err) => {});
  }, []);

  return (
    <>
      <Banner />
      <ListCategory />
      <FlashDeals title="Flash Deals" />
      <TopProducts title="Top tìm kiếm" topProducts={topProducts} />
      <div className="maxwidth">
        <ListProducts title="Daily discover" products={products} />
      </div>
    </>
  );
};

const topProducts = [
  {
    href: "khi-hoi-tho-hoa-thinh-khong",
    title: "Hỗ trợ khí thở",
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/media/catalog/product/k/h/khi-hoi-tho-hoa-thinh-khong.u5464.d20170726.t170655.288851.jpg",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ba/80/a3/3eee5b22e9f3963da9c9517caf342ed3.jpg",
    title: "Máy rửa chén",
    href: "may-rua-bat-electrolux-esf6010bw-hang-chinh-hang",
  },
  {
    href: "khi-hoi-tho-hoa-thinh-khong",
    title: "Hỗ trợ khí thở",
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/media/catalog/product/k/h/khi-hoi-tho-hoa-thinh-khong.u5464.d20170726.t170655.288851.jpg",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ba/80/a3/3eee5b22e9f3963da9c9517caf342ed3.jpg",
    title: "Máy rửa chén",
    href: "may-rua-bat-electrolux-esf6010bw-hang-chinh-hang",
  },
  {
    href: "khi-hoi-tho-hoa-thinh-khong",
    title: "Hỗ trợ khí thở",
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/media/catalog/product/k/h/khi-hoi-tho-hoa-thinh-khong.u5464.d20170726.t170655.288851.jpg",
  },
  {
    imgSrc:
      "https://salt.tikicdn.com/cache/280x280/ts/product/ba/80/a3/3eee5b22e9f3963da9c9517caf342ed3.jpg",
    title: "Máy rửa chén",
    href: "may-rua-bat-electrolux-esf6010bw-hang-chinh-hang",
  },
];

const productDeals = [
  {
    href: "#",
    sale: "10%",
    price: 100000,
    promotion: "",
    salePercent: 100,
    srcImg: "../images/products/3520328350801_s_01.d20170825.t114520.75128.jpg",
    barLoad: 30,
  },
  {
    href: "#",
    sale: "30%",
    price: 50000,
    promotion: "",
    salePercent: 90,
    srcImg: "../images/products/3520328350801_s_01.d20170825.t114520.75128.jpg",
    barLoad: 40,
  },
  {
    href: "#",
    sale: "70%",
    price: 200000,
    promotion: "",
    salePercent: 80,
    srcImg: "../images/products/3520328350801_s_01.d20170825.t114520.75128.jpg",
    barLoad: 60,
  },
  {
    href: "#",
    sale: "70%",
    price: 200000,
    promotion: "",
    salePercent: 80,
    srcImg: "../images/products/3520328350801_s_01.d20170825.t114520.75128.jpg",
    barLoad: 70,
  },
  {
    href: "#",
    sale: "10%",
    price: 100000,
    promotion: "",
    salePercent: 100,
    srcImg: "../images/products/3520328350801_s_01.d20170825.t114520.75128.jpg",
    barLoad: 80,
  },
  {
    href: "#",
    sale: "30%",
    price: 50000,
    promotion: "",
    salePercent: 90,
    srcImg: "../images/products/3520328350801_s_01.d20170825.t114520.75128.jpg",
    barLoad: 90,
  },
];

const mapDispatchToPro = {
  loadProduct,
};
const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};
export default connect(mapStateToProps, mapDispatchToPro)(Home);
