import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const use = useSelector((state) => state.user);
  return (
    <Route
      {...rest}
      render={(props) =>
        use?.name ? <Component {...props} /> : 
        <Redirect to={{pathname: "/login", state: {path: rest.path}}} />
      }
    />
  );
};

export default PrivateRoute;
