import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import "./Narbar.scss";

const Narbar = () => {
  const [input, setInput] = useState("");
  const user = useSelector((state) => state.user);
  return (
    <div className="whole-bar">
      <div className="maxwidth flex narbar">
        <Link to={"/"}>
          <div className="logo">4 YOU</div>
        </Link>
        <div className="bar">
          <div className="bar1">
            <div to="/" className="item float-left wrap-popup">
              Notification
              <div className="notification-popup">
                <h3> News <span className="float-right btn-close">X</span> </h3>
                <ul>
                  <li><Link to="ahihi">ahihi</Link> </li>
                  <li><Link to="ahihi">ahihi</Link> </li>
                  <li><Link to="ahihi">ahihi</Link> </li>
                  <li><Link to="ahihi">ahihi</Link> </li>
                </ul>
                <Link to="ahihi" ><h4> Show more</h4></Link>
              </div>
            </div>
            {user.name ? (
              <Link to="/hidha/dsadsad/dsdsad" className="item float-right">
                {user.name}
              </Link>
            ) : (
              <>
                <Link to="/signup" className="item float-right">
                  Sign up
                </Link>
                <Link to="/login" className="item float-right">
                  Login
                </Link>
              </>
            )}
          </div>
          <div className="bar2 flex clear-float">
            <form className="input-search flex-10">
              <input
                value={input}
                onChange={(e) => setInput(e.target.value)}
                className="full-width"
                type="text"
                placeholder="Nhập tìm kiếm..."
              />
              <Link to={`/search?keyword=${input}`}>Search</Link>
            </form>
            <Link to="/shopping_cart" className="item flex-1 text-center">
              Card
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

Narbar.propTypes = {
  hiddenBar: PropTypes.string,
  valueSearch: PropTypes.string,
};

export default Narbar;
