import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "./ListProducts.scss";

const ListProducts = ({ title = "Nothing", products }) => {
  return (
    <div className="fullwidth list-product">
      <h2>{title}</h2>
      <div className="container-item-product flex-wrap">
        {products.map((c, index) => (
          <div key={index} className="product-item">
            <Link className="wrap-product-item" to={`/products/${c.id}`}>
              {c.sale ? <p className="sale-percent">{c.sale}% OFF</p> : ""}
              <div
                className="wrap-img"
                style={{
                  backgroundImage: `url(${c.imgSrc})`,
                }}
              ></div>
              <p className="product-name">{c.name}</p>
              <div className="flex" style={{ alignItems: "center" }}>
                <p className="price">
                  {(c.price.toFixed(0).toString() + "000").replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  ₫
                </p>
                {c.price > 100000000 ? "" : <p className="sole">đã bán 10</p>}
              </div>
            </Link>
            <div className="more-item">
              <button>Thêm vào yêu thích</button>
            </div>
          </div>
        ))}
      </div>
      <div className="btn-xem-them">
        <Link to="/">Xem thêm</Link>
      </div>
    </div>
  );
};

ListProducts.propTypes = {
  products: PropTypes.arrayOf(Object).isRequired,
  imgSrc: PropTypes.string,
  title: PropTypes.string,
  price: PropTypes.string,
  sale: PropTypes.string,
  priceSale: PropTypes.string,
  slugg: PropTypes.string,
  category: PropTypes.string,
  id: PropTypes.number,
};

export default ListProducts;
