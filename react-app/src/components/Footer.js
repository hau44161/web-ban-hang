import React from "react";
import "./Footer.scss";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <div className="whole-footer">
      <div className="maxwidth container-footer">
        <div className="customer-care flex">
          <ul>
            <li>
              CHĂM SÓC KHÁCH HÀNG
            </li>
            <li>
              <Link to="/">Trung Tâm Trợ Giúp</Link>
            </li>
            <li>
              <Link to="/">Hướng dẫn bán hàng</Link>
            </li>
            <li>
              <Link to="/">Hướng dẫn mua hàng</Link>
            </li>
            <li>
              <Link to="/">Thanh toán</Link>
            </li>
            <li>
              <Link to="/">Vận chuyển</Link>
            </li>
            <li>
              <Link to="/">Chính sách bảo hành</Link>
            </li>
          </ul>
          <ul>
            <li>
              Về 4 YOU
            </li>
            <li>
              <Link to="/">Giới thiệu 4 YOU</Link>
            </li>
            <li>
              <Link to="/">Tuyển dụng</Link>
            </li>
            <li>
              <Link to="/">Điều khoản sử dụng</Link>
            </li>
            <li>
              <Link to="/">Chính sách bảo mật</Link>
            </li>
            <li>
              <Link to="/">Bán hàng doanh nghiệp</Link>
            </li>
          </ul>
          <ul className="thanh-toan">
            <li>Thanh Toán</li>
            <li></li>
          </ul>
          <ul>
            <li>Liên kết</li>
            <li><Link to="/">Facebook</Link></li>
            <li><Link to="/">Youtube</Link></li>
            <li><Link to="/">Instagram</Link></li>
          </ul>
        </div>
        <div className="copy-right">
          <p>Copyright &copy; 2020 by Hau Ngo</p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
