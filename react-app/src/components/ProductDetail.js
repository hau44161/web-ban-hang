import React from 'react'
import "./ProductDetail.scss";

const ProductDetail = ({detail=[]}) => {
  return (
    <div className="thong-tin-chi-tiet-sp">
      <h3 className="danh-rieng-title">Thông tin chi tiết</h3>
      <table>
        <tbody>
          {detail.map((c, index) => (
            <tr key={index}>
              <td>{c.title}</td>
              <td>{c.value}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default ProductDetail;