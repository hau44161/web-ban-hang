import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "./FlashDeals.scss";
import { TimeDown } from "./TimeDown";
import { loadProductFlashDeal } from "../redux/actions/actions";
import { connect } from "react-redux";

const FlashDeals = ({ title, productDeals, loadProductFlashDeal }) => {
  useEffect(() => {
    if (!productDeals[0])
    loadProductFlashDeal();
  }, []);
  return (
    <div className="maxwidth list-flashdeal">
      <h2>
        {title} <TimeDown hours={20} />{" "}
      </h2>
      <div className="container-item-flash flex-wrap">
        {productDeals && productDeals.map((c, index) => (
          <Link key={index} className="flash-item" to={`products/${c.id}`}>
            <p className="sale-percent">10% OFF</p>
            <div
              className="wrap-img"
              style={{
                backgroundImage: `url(${c.imgSrc})`,
              }}
            ></div>
            <p className="price">
              {(c.price
                .toFixed(0)
                .toString()+"000")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
              đ
            </p>
            <div className="load-bar">
              <div className="da-ban" style={{ width: `${c.barLoad}%` }}>
                đã bán
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

FlashDeals.propTypes = {
  productDeals: PropTypes.arrayOf(Object),
  href: PropTypes.string,
  sale: PropTypes.string,
  price: PropTypes.number,
  promotion: PropTypes.string,
  salePercent: PropTypes.number,
  srcImg: PropTypes.string,
  barLoad: PropTypes.number,
};
const mapDispatchToProps = {
  loadProductFlashDeal,
};
const mapStateToProps = (state) => ({
  productDeals: state.productFlashDeal
});
export default connect(mapStateToProps, mapDispatchToProps)(FlashDeals);
