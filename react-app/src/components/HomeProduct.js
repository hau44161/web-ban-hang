import React from "react";
//import { useParams } from "react-router-dom";
import ProductImg from "./ProductImg";
import ProductDetail from "./ProductDetail";
import ProductQA from "./ProductQA";
import ProductComment from "./ProductComment";

//import PropTypes from 'prop-types'

const HomeProduct = (props) => {

  return (
    <div className="whole-home-product">
      <div className="maxwidth container-home-product">
        <ProductImg />
        <ProductDetail />
        <ProductQA />
        <ProductComment />
      </div>
    </div>
  );
};

HomeProduct.propTypes = {};

export default HomeProduct;
