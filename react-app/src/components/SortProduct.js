import React from "react";
//import PropTypes from "prop-types";
import "./SortProduct.scss";

const SortProduct = () => {
  return (
    <div className="whole-sort-product">
      <ul>
        <li>Sắp xếp theo</li>
        <li className="active" onClick={()=>{console.log("is active")}}><button className="btn-default">Liên quan</button></li>
        <li onClick={()=>{console.log("is active")}}><button className="btn-default">Mới nhất</button></li>
        <li onClick={()=>{console.log("is active")}}><button className="btn-default">Bán chạy</button></li>
        <li onClick={()=>{console.log("is active")}}><button className="btn-default">Giá</button></li>
      </ul>
    </div>
  );
};

SortProduct.propTypes = {};

export default SortProduct;
