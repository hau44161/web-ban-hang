import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./ProductImg.scss";
import {
  loadProduct,
  loadProductDetailById,
  loadProductById,
  addCart,
} from "../redux/actions/actions";
import { useParams, withRouter } from "react-router-dom";

const ProductImg = ({
  product = {},
  productDetail,
  loadProductDetailById,
  loadProductById,
  addCart,
}) => {
  const { productId: idProduct } = useParams();
  const [count, setCount] = useState(1);

  useEffect(() => {
    if (!product?.name) loadProductById(idProduct);
    if (!productDetail.color) loadProductDetailById(idProduct);
  }, []);

  const handleCart = () => {
    addCart({ count, idProduct });
  };
  const star = () => {
    let arr = [1, 2, 3, 4, 5];
    return arr.map((c, index) =>
      c <= productDetail.star ? (
        <li key={index}></li>
      ) : (
        <li key={index} style={{ background: "white" }}></li>
      )
    );
  };

  return (
    <div className="xem-sp">
      <div className="xem-hinh">
        <ul className="ds-hinh-anh">
          {productDetail?.imgSrc &&
            productDetail.imgSrc.map((c, index) => (
              <li key={index}>
                <img src={c} alt="" />
              </li>
            ))}
        </ul>
        <div className="hinh-chinh">
          {product?.imgSrc && <img src={product.imgSrc} alt="" />}
        </div>
      </div>
      <div className="thong-tin-co-ban">
        <h2 className="product-name">{product.name}</h2>
        <ul className="danh-gia mobile-hidden tablet-hidden">
          {productDetail.star && star()}
          <li className="so-nhan-xet tablet-hidden">
            ({(productDetail?.comment && productDetail.comment.length) || 0}{" "}
            nhận xét)
          </li>
        </ul>
        <h3 className="product-price-sale">
          {(product.price + "000").replace(/\B(?=(\d{3})+(?!\d))/g, ",")} đ
          {/* {product.price} */}
        </h3>
        <p className="product-price">{product.price}</p>
        <ul className="uu-diem-sp">
          {productDetail?.describer &&
            productDetail.describer.map((c, index) => <li key={index}>{c}</li>)}
        </ul>
        <div className="chon-mau-sac">
          <span>Chọn màu sắc: </span>
          <ul>
            {productDetail?.color && <li>{productDetail.color}</li>}
            {/* productDetail.color.map((c, index) => <li key={index}>{c}</li>)} */}
          </ul>
        </div>
        <div className="chon-so-luong">
          <p>Số lượng:</p>
          <div className="group-btn-so-luong">
            <p onClick={() => count > 1 && setCount(count - 1)}>-</p>
            <p>{count}</p>
            <p onClick={() => setCount(count + 1)}>+</p>
          </div>
          <button className="btn-mua" onClick={handleCart}>
            Chọn mua
          </button>
        </div>
      </div>
    </div>
  );
};

ProductImg.propTypes = {
  productDetail: PropTypes.object,
  product: PropTypes.object,
  loadProductDetailById: PropTypes.func,
  loadProduct: PropTypes.func,
  loadCart: PropTypes.func,
  addCart: PropTypes.func,
};

const mapDispatchToProps = {
  loadProduct,
  loadProductById,
  loadProductDetailById,
  addCart
};

const mapStateToProps = (state, ownProps) => ({
  productDetail: state.productDetail.find(c=> c.idProduct == ownProps.match.params.productId) || {},
  product:
    state.products.find((p) => p.id == ownProps.match.params.productId) || {},
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ProductImg)
);
