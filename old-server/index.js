require("dotenv").config();

const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;



const homeRouter = require("./routers/home.route");
const loginRouter = require("./routers/auth.route");
const productRouter = require("./routers/products.route");
const cardRouter = require("./routers/card.route");

const authMiddlewares = require("./middlewares/auth.middleware");
const sessionMiddleware = require("./middlewares/session.middleware");

const app = express();
const port = 4000;

app.set("view engine", "pug");
app.set("views", "./views");

app.use(cookieParser(process.env.SESSION_SECRET));
app.use(express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(sessionMiddleware.session);

app.use("/card", cardRouter);
app.use("/home", productRouter);
app.use("/home", authMiddlewares.requiredAuth, homeRouter);
app.use("/auth", loginRouter);

//-----------------------------------------

app.get("/", (req, res) => {
  res.send(
    "<h1>hello world, ahihi</h1><a href='/home'>Home</a><a href='/home/create'>Create</a>"
  );
});

app.listen(port, () => {
  console.log(`server at port ${port} is running...`);
});
