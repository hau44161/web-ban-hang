const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("db.json");
const db = low(adapter);

db.defaults({
  names: [
    { name: "Hau Ngo", phone: 012324512, id: 1 },
    { name: "Hien Ngo", phone: 012324512, id: 2 },
    { name: "Hieu Ngo", phone: 012324512, id: 3 },
  ],
}).write();

module.exports = db;
