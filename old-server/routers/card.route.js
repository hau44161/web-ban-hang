const express = require("express");
const router = express.Router();

const controller = require('../controllers/card.controller');

router.get("/add/:idProduct", controller.addToCard);

module.exports=router;