const express = require("express");
const multer  = require('multer')
const router = express.Router();

const validate = require("../validates/user.validate.js");
const controller = require("../controllers/home.controller");
var upload = multer({ dest: './public/uploads'})
 

router.get("/", controller.index);

router.get("/create", controller.getCreate);

router.get("/user/:id", controller.getUserById);

router.post("/create", upload.single('avatar'), validate.createUser, controller.postCreate);

router.get("/search", controller.getSearch);



module.exports = router;