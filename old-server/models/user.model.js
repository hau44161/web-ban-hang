const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
  name: String,
  phone: String,
  image: String,
  password: String,
  email: String,
});

let Users = mongoose.model("Users", usersSchema, "users");

module.exports = Users;
