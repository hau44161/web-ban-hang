const db = require("../db");
const Users = require("../models/user.model");

module.exports.index = async (req, res) => {
  let users = await Users.find();
  res.render("index", {
    names: users,
    input: "",
  });
};

module.exports.getCreate = (req, res) => {
  res.render("create", {});
};

module.exports.getUserById = async (req, res) => {
  let idUser = req.params.id;
  const kq = await Users.find({ id: idUser });
  console.log(kq);
  res.render("user", { name: kq.name, index: kq.id });
};

module.exports.getSearch = async (req, res) => {
  let input = req.query.name;
  let names = await Users.find({ name: input });
  if (names)
    res.render("index", {
      names,
      input,
    });
};

module.exports.postCreate = async (req, res) => {
  let item = {
    name: req.body.name,
    phone: req.body.phone,
    email: req.body.email,
    password: req.body.password,
    image: req.file.path.split("/").slice(1).join("/")
  };
  await Users.create(item);
  res.redirect("/home");
};
