const db = require('../db');

module.exports.addToCard=(req, res)=>{
  let idProduct = req.params.idProduct;
  let idSession = req.signedCookies.sessionId;

  if(!idSession){
    res.redirect("/home/products")
    return;
  }

  let counter = db.get("sessions")
    .find({id: idSession})
    .get("card."+idProduct, 0).value();

  db.get("sessions")
    .find({id: idSession})
    .set('card.'+idProduct, counter+1)
    .write();

  res.redirect('/home/products')
}