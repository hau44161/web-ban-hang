const Product = require("../models/product.model");

module.exports.getProducts = async (req, res) => {
  // debugger;
  // let perPage = 6;
  // let page = req.query.page || 1;

  // let start = (page - 1) * perPage;
  // let end = page * perPage;

  // let maxPage = Math.ceil(db.get("products").value().length / page);
  // let currPage;
  // if (page == 1) currPage = 1;
  // else if (page == maxPage - 1) currPage = page - 2;
  // else currPage = page - 1;

  // res.render("products", {
  //   products: db.get("products").value().slice(start, end),
  //   pagi: [currPage, currPage + 1, currPage + 2],
  // });
  let products = await Product.find();

  res.render("products", {
    products: products,
    pagi: [0, 1, 2],
  });
};