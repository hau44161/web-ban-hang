const db = require("../db");

module.exports.login = (req, res) => {
  res.render("login");
};
module.exports.postLogin = (req, res) => {
  let input = req.body;
  let user = db.get("names").find({ email: input.email }).value();
  if (!user) {
    res.render("login", { errors: "user does not exist.", values: input });
    return;
  }
  if (user.password != input.password) {
    res.render("login", { errors: "password is wrong.", values: input });
    return;
  }
  
  res.cookie("userId", user.id, {signed: true})
  res.redirect('/home')
};
