const db = require("../db");

module.exports.session = (req, res, next) => {
  if (!req.signedCookies.sessionId) {
    res.cookie("sessionId", "hdjahf2f51", { signed: true });
    db.get("sessions")
      .push({
        id: "hdjahf2f51",
      })
      .write();
  }

  res.locals.totalCard = Object.values(
    db.get("sessions")
    .find({ id: req.signedCookies.sessionId })
    .value().card
  ).reduce((c, value) => (c += value));
  
  next();
};
