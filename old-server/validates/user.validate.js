module.exports.createUser = (req, res, next) => {
  let input = req.body;
  let errors = [];
  if (!input.name) errors.push("name is requied.");
  if (!input.phone) errors.push("phone is requied.");
  if (errors.length){
    res.render('create', {
      errors: errors,
      values: input
    })
    //res.local.value=input;
    return;
  }
  next();
};
