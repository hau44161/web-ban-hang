This app use [Create React App](https://github.com/facebook/create-react-app).

# Feature

### Cart
Add item to cart<br/>
Edit amount<br/>
Delete item from cart
<hr/>

### Login
Just have only login
<hr/>

### Product 
Just search product by click search button 
<hr/>

### UI make by myself
<hr/>

# How to use
## Requied
Your computer have to install Docker and Docker-compose

## Step

### 1. Run the docker container

```bash
docker-compose up
```
### 2. Execute query 

```bash
docker exec -it web-ban-hang_db_1 bash -c "mysql -uroot -phaungo -e 'source /usr/db/web-ban-hang.sql;'"
```

# Checkout the website running
> check the site url: http://127.0.0.1:3000/
### Initialized information

```yaml
Account:
    user: 1851050043hau@ou.edu.vn
    pass: ahihi
```
### Checkout the server running

> check the site url: http://127.0.0.1:2000/

### Checkout the mysql running

> check the url: http://127.0.0.1:3308/